<?php echo $header; ?>
<div class="container">
  <!--<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>-->
  <div class="row">
    
    <div id="content" class="col-sm-12"><!--<?php echo $content_top; ?> -->
      <div class="row">
        <div class="col-sm-12">
            <h1><?php echo $heading_title; ?></h1>
        </div>
        <div class="col-sm-4">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail" id="popup_thumb_img" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img id="thumb_img" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          
          <?php } ?>
          
        </div>
        <div class="col-sm-8" id="product">
                <span class='glassesHref' style='display:none;'><?= $glasses[0]['href']; ?></span>
                <div class="col-sm-6">
                  <?php if ($options) { ?>
                      <?php foreach ($options as $option) { ?>
                          <?php if ($option['type'] == 'select') { ?>
                          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>" style="color:black;"><?php echo $option['name']; ?></label><br>
                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow <?php if($option['option_id'] == '17'){ echo ' glassesChange '; } ?>">
                              <?php foreach ($option['product_option_value'] as $option_value) { ?>
                              <option data-hint="<?=$option_value['hint'];?>" data-url="<?php echo $option_value['variation_url']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                              <?php if ($option_value['price']) { ?>
                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                              <?php } ?>
                              </option>
                              <?php } ?>
                            </select>
                              <div class="categ-show-hint"></div>
                          </div> 
                          <?php } ?>
                      <?php } ?>
                  <?php } ?>
                </div>
                <div class="col-sm-6">
                <?php if ($options) { ?>
                    <?php foreach ($options as $option) { ?>
                    <!--<?php if ($option['type'] == 'select') { ?>
                          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label><br>
                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($option['product_option_value'] as $option_value) { ?>
                              <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                              <?php if ($option_value['price']) { ?>
                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                              <?php } ?>
                              </option>
                              <?php } ?>
                            </select>
                          </div> 
                          <?php } ?> -->
                        <?php if ($option['type'] == 'radio') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label"><?php echo $option['name']; ?></label>
                          <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="radio">
                              <label>
                                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                <?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                              </label>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <?php } ?>                        
                        <?php if ($option['type'] == 'image') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-bottom: 5px">
                        <div class="row">
                          <label class="control-label" style="color:black;"><?php echo $option['name']; ?></label>
                          <div id="input-option<?php echo $option['product_option_id']; ?>">
                                <?php foreach ($option['product_option_value'] as $keyI => $option_value) { ?>
                               
                                    <?php if($keyI == 0){ ?>
                                        <div style='float:left;padding-left:5px;'>
                                          <label class='productColor' data-parentOption_id="<?php echo $option['option_id']; ?>" data-option_id="<?= $option_value['option_value_id']; ?>" data-product-image="<?=$option_value['product_image'];?>" data-popup-product-image="<?=$option_value['popup_product_image'];?>">
                                              <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" checked="true" style='display:none;'/>
                                              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail"  style='height:27px;' data-toggle="tooltip" data-placement="bottom" title="<?php echo $option_value['name'] ?>" />
                                          </label>
                                        </div>
                                    <?php }else{ ?>
                                    <div style='float:left;padding-left:5px;' >
                                      <label class='productColor'  data-parentOption_id="<?php echo $option['option_id']; ?>" data-option_id="<?= $option_value['option_value_id']; ?>" data-product-image="<?=$option_value['product_image'];?>" data-popup-product-image="<?=$option_value['popup_product_image'];?>">
                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" style='display:none;'/>
                                        <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  data-toggle="tooltip" title="<?php echo $option_value['name'] ?>" data-placement="bottom"  style='height:27px;'  /> 
                                      </label>
                                    </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                          </div>
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'text') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'textarea') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'file') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label"><?php echo $option['name']; ?></label>
                          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'date') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                          <div class="input-group date">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'datetime') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                          <div class="input-group datetime">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'time') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                          <div class="input-group time">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <!--<?php if ($recurrings) { ?>
                <hr>
                <h3><?php echo $text_payment_recurring ?></h3>
                <div class="form-group required">
                  <select name="recurring_id" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($recurrings as $recurring) { ?>
                    <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                    <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
                </div>
                <?php } ?> -->
            </div>
            <div class="col-sm-12">
               <div class="row">
                   <div class="col-sm-6 col-xs-12">
                       <div class="col-sm-12 col-xs-12 blockWithoutComplectP blockComplectArrowP" style="cursor:pointer;border:2px solid #fab446 !important;">
                        <h4>Полотно</h4>
                        <div class='row'>
                            <div class="col-sm-6">
                            <?php if ($price) { ?>
                                <ul class="list-unstyled">
                                  <?php if (!$special) { ?>
                                  <li>
                                    <h2 style='margin-top: 0px' class='priceChange'><b><?php echo $price; ?></b></h2>
                                  </li>
                                  <?php } else { ?>
                                  <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
                                  <li>
                                    <h2 style='margin-top: 0px' class='priceChange'><b><?php echo $special; ?></b></h2>
                                  </li>
                                  <?php } ?>
                                  <?php if ($tax) { ?>
                                  <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
                                  <?php } ?>
                                  <?php if ($points) { ?>
                                  <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                                  <?php } ?>
                                  <?php if ($discounts) { ?>
                                  <li>
                                    <hr>
                                  </li>
                                  <?php foreach ($discounts as $discount) { ?>
                                  <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                  <?php } ?>
                                  <?php } ?>
                                </ul>
                            <?php } ?>
                            </div>
                            <div class="col-sm-6  quantityWithoutComplect">
                                <div class="input-group parent_block_cart">
                                    <span class="input-group-btn">
                                        <button class="dec-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                    </span>
                                    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                    <span class="input-group-btn">
                                        <button class="inc-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                    </span>
                                </div>
                            </div>
                            </div>
                            <p>Включает полотно</p>
                    </div>
                    </div>
                   
                    <?php if ($options) { ?>
                     <?php foreach ($options as $option) { ?>
                         <?php if ($option['type'] == 'checkbox') { ?>
                         <div class="col-sm-6 col-xs-12">
                         <div class="col-sm-12 col-xs-12 blockWithComplectP" style="cursor:pointer;border: 2px solid #e2e2e2;">
                             <h4>Комплект</h4>
                             <?php if ($options) { ?>
                                 <?php $optionPrice = ''; ?>
                                 <?php foreach ($options as $option) { ?>
                                     <?php if ($option['type'] == 'checkbox') { ?>
                                             <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                              <?php if ($option_value['price']) { ?>
                                                 <?php $optionPrice  = $option_value['price']; ?>
                                                 <?php } ?>
                                             <?php } ?>
                                     <?php } ?>
                                 <?php } ?>
                             <?php } ?>
                             <div class='row'>
                             <div class="col-sm-6">
                                <?php if ($price) { ?>
                                    <ul class="list-unstyled">
                                      <?php if (!$special) { ?>
                                      <li>
                                        <h2 style='margin-top: 0px'><b class='priceChange'><?php echo $price + $optionPrice; ?>₽</b></h2>
                                      </li>
                                      <?php } else { ?>
                                      <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
                                      <li>
                                        <h2 style='margin-top: 0px'><b class='priceChange'><?php echo $special; ?></b></h2>
                                      </li>
                                      <?php } ?>
                                      <!--<?php if ($tax) { ?>
                                      <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
                                      <?php } ?>-->
                                      <?php if ($points) { ?>
                                      <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                                      <?php } ?>
                                      <?php if ($discounts) { ?>
                                      <li>
                                        <hr>
                                      </li>
                                      <?php foreach ($discounts as $discount) { ?>
                                      <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                      <?php } ?>
                                      <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6 quantityWithComplect"></div>
                            </div>
                             <div class="col-sm-12" style="padding:0px;">
                             <?php if ($options) { ?>
                                 <?php foreach ($options as $option) { ?>
                                     <?php if ($option['type'] == 'checkbox') { ?>
                                         <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                           <div id="input-option<?php echo $option['product_option_id']; ?>">
                                             <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                             <div class="checkbox">
                                               <label style="padding-left:0px;">
                                                 <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                 <?php echo $option_value['name']; ?>
                                               </label>
                                             </div>
                                             <?php } ?>
                                           </div>
                                         </div>
                                     <?php } ?>
                                 <?php } ?>
                             <?php } ?>
                             </div>    
                         </div>
                         </div>
                         <?php } ?>
                         <?php } ?>
                     <?php } ?>
            </div>
                <div class="col-sm-12" style="padding:0px;">
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                </div>
                <div class="row" style="padding-top: 10px;padding-bottom: 10px">
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 blockWithoutComplectPButton" style="padding:0px;">
                        <div class="col-xs-6 col-sm-6" style="padding:0px;">
                            <button type="button" id="callmodal" class="btn pull-right" data-product="<?php echo $heading_title; ?>" data-stock="<?=$stock?>" data-price="<?php echo $price; ?>" data-special="<?=$special?>" data-href="<?php echo $breadcrumbs[count($breadcrumbs)-1]['href']; ?>" data-src="<?php echo $image['thumb']; ?>" style="background-color: #e1e2e6;border-radius: 0px; color:black;" ><span style="font-size:16px;">Купить в 1 клик</span></button>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn pull-right" style="color:white;border:0px solid transparent;border-radius: 0px;background-color: #f15923;height:39px;margin-left: 5px"><span style="font-size:16px;">В корзину</span></button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 blockWithComplectPButton" style="padding:0px;">
                    </div>
                </div>

                <?php if ($minimum > 1) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                <?php } ?>
            </div>
          
            <div class="col-sm-12" style="padding:0px;">
                <div class="col-sm-3 col-xs-6" style="padding:0px;">
                    <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:20px;width:20px;float:left;"></div><h4 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>Сравнить</h4></a>
                </div>
                <div class="col-sm-3 col-xs-6" style="padding:0px;">
                    <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product_id; ?>');"><div class="header_heart_icon" style="height:20px;width:20px;float:left;"></div><h4 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>В желания</h4></a>
                </div>
                <div class="col-sm-3 hidden-xs" style="padding:0px;">
                    <a  data-toggle="tooltip" class="you_have_qustion_button" title="Нашли дешевле" id="foundCheaper" data-product="<?php echo $heading_title; ?>" data-stock="<?=$stock?>" data-price="<?php echo $price; ?>" data-special="<?=$special?>" data-href="<?php echo $breadcrumbs[count($breadcrumbs)-1]['href']; ?>" data-src="<?php echo $image['thumb']; ?>" style="cursor:pointer;" ><div class="icon_dolar" style="height:20px;width:10px;float:left;"></div><h4 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>Нашли дешевле</h4></a>
                    <div class="you_have_qustion">
                        <i class='fa fa-times fa-3x you_have_qustion_button pull-right' style='cursor:pointer;'></i>
                        <div class="form-group">
                            <div class="found_cheaper" id="found_cheaper">
                                <form name="foundCheaperForm" id="foundCheaperForm">
                                    <span style="text-decoration: line-through;"><p id="oldPriceLabel"></p></span>
                                    <input type="hidden" class="form-control" value="" id="productValue" />
                                    <input type="hidden" class="form-control" value="" id="priceValue" />
                                    <div class="col-sm-12" style="text-align: center">
                                        <h2 style="color:black;margin-top: 0px">Вернём разницу</h2>
                                    </div>
                                    <p>Вы получите скидку, если у конкурента в настоящее время данный товар есть в наличии, цена действующая, и товар конкурента сертифицирован и имеет гарантию производителя на территории Российской Федерации</p>
                                    <div class="col-sm-12 modal-title text-overflow" id="productLabel" style="background-color: #f4f2f2;font-size: 12px;padding-top:5px;padding-bottom:5px;margin-top: 5px;margin-bottom: 5px"></div>
                                    <div class="col-sm-12">
                                        <h2 style="color:black;margin-top: 5px"><b id='priceLabel'></b></h2>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4 control-label" for="inputSuccess4">Где дешевле</label>
                                                <div class="col-sm-8">
                                                    <input type="url" class="form-control" placeholder="" id="url_cheaper" required data-validation-required-message="Обязательное поле" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-bottom:15px;">
                                            <div class="row">
                                                <label class="col-sm-4 control-label" for="inputSuccess4">Цена конкурента</label>
                                                <div class="col-sm-8">
                                                    <input type="number" class="form-control" placeholder="" id="priceCheaper" required data-validation-required-message="Обязательное поле" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4 control-label" for="inputSuccess4">Имя</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" placeholder="" id="nameValue" required data-validation-required-message="Обязательное поле" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4 control-label" for="inputSuccess4">Телефон</label>
                                                <div class="col-sm-8">
                                                    <input type="phone" class="form-control" placeholder="" id="phoneValue" required data-validation-required-message="Обязательное поле" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!--<input type="email" class="form-control" placeholder="Email" id="emailCustomer"  />
                                    <!--<textarea rows="10" cols="100" class="form-control" placeholder="Ваше вопрос" id="message"  maxlength="999" style="resize:none"></textarea> -->
                                    <div class="col-sm-8 col-md-offset-4" >
                                        <button type="submit" class="btn" style="background-color: #2a307b; color:white;border-radius: 0px">Отправить</button>
                                    </div>
                                </form>
                                <div id="success"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 hidden-xs" style="padding:0px;">
                    <a  data-toggle="tooltip"  title="Появились вопроси?"  style="cursor:pointer;" ><div class="icon_you_have_question" style="height:20px;width:20px;float:left;"></div><h4 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>Появились вопроси?</h4></a>
                </div>
            </div>
            <div id="hide_map"></div>
            <div style="clear: both;"></div>
            <div class="row delivery_info hidden-xs">
                <div class="col-sm-3" style="padding:0px;">
                    <div class="delivery_content">
                        <p>Доставка в <span id="delivery_city"></span></p>
                        <p>Срок доставки <strong>2 дня</strong></p>
                    </div>
                </div>
                <div class="col-sm-3" style="padding:0px;">
                    <div class="delivery_content">
                        <p>Цена <span id="delivery_price"></span><p>
                        <p>Оплата <strong>при получении</strong></p>
                    </div>
                </div>
                <div class="col-sm-6" style="padding:0px;">
                    <div class="delivery_text">
                        Если товар не подошел,<br>
                        вернём деньги в течение 7 дней<br>
                        с момента его получения
                    </div>

                </div>
            </div>
            
          </div>
          
      </div>
      
      <!--<?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>-->

      </div>
    </div>
    
    <div class="row">
       <ul class="nav nav-tabs  menu_comparison_list" style='background-color: #f4f2f2'>
            <!--<li ><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li> -->
            <?php if ($attribute_groups) { ?>
            <li class="active col-xs-12 col-sm-auto col-md-auto col-lg-auto" style="padding:0px;"><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <!--<?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?> -->
            <?php if ($pagonazhs) { ?>
            <li class="col-xs-12 col-sm-auto col-md-auto col-lg-auto" style="padding:0px;"><a href="#tab-pagonazhs" data-toggle="tab">Погонаж</a></li>
            <?php } ?>
            <?php if ($products) { ?>
            <li class="col-xs-12 col-sm-auto col-md-auto col-lg-auto" style="padding:0px;"><a href="#tab-products" data-toggle="tab">Сопутствующие товары</a></li>
            <?php } ?>
            <?php if ($services) { ?>
            <li class="col-xs-12 col-sm-auto col-md-auto col-lg-auto" style="padding:0px;"><a href="#tab-services" data-toggle="tab">Услуги по установке</a></li>
            <?php } ?>
          </ul>
          
          <div class="tab-content">
            <!--<div class="tab-pane" id="tab-description"><?php echo $description; ?></div>-->
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane active" id="tab-specification">
                <div class="row">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="col-sm-4 col-xs-6">
                                        <h4><?php echo $attribute['name']; ?>:</h4>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                        <h4><?php echo $attribute['text']; ?></h4>
                                    </div>
                                </div>
                            <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
            
            
            
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            
            
            <div class="tab-pane" id="tab-pagonazhs">
            <?php if ($pagonazhs) { ?>
                    <div class='row'>
                    <div class='col-sm-12' style='padding-top:15px;padding-bottom:15px;'>
                        <div style='float:left;'>
                        <span style='border-bottom:2px dashed #fab446;font-size: 15px'><b>Коробка и наличники</b></span>
                        </div>
                        <div class='icon_arrow_pogonazh' style='float:left;height:20px;width: 20px;margin-left: 5px'></div>
                    </div>
                    <div class='col-sm-12 PogonazhKorobka' >
                        <div class="hidden-xs col-sm-12" style="background-color: #f4f2f2;font-size: 12px;padding-top:5px;padding-bottom:5px;margin-top: 5px;margin-bottom: 5px">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">Наименование работ</div>
                            <div class="col-sm-1">Цвет</div>
                            <div class="col-sm-2">Размеры,мм</div>
                            <div class="col-sm-2">Единицы измерения</div>
                            <div class="col-sm-2">Количество</div>
                            <div class="col-sm-1">Цена</div>
                            <div class="col-sm-1"></div>
                        </div>
                      <?php foreach ($pagonazhs as $pagonazh) { ?>
                        <?php if($pagonazh['category_id'] == '70'){ ?>
                                  <div class="col-sm-12 hidden-xs pogonazhProduct" style="border-bottom: 1px solid #e2e2e2;margin-top: 20px;padding-bottom: 10px" id='product_ser_<?= $pagonazh["product_id"];?>'>
                              <div class="col-sm-1">
                                  <div class="image">
                                      <a href="<?php echo $pagonazh['href']; ?>">
                                          <img src="<?php echo $pagonazh['thumb']; ?>" alt="<?php echo $pagonazh['name']; ?>" title="<?php echo $pagonazh['name']; ?>" class="img-responsive" />
                                      </a>
                                  </div>
                              </div>

                              <div class="col-sm-2">
                                  <?php echo $pagonazh['name']; ?>
                              </div>
                              <div class="col-sm-1">
                                  <?php if ($pagonazh['options']) { ?>
                                      <?php foreach ($pagonazh['options'] as $option) { ?>
                                          <?php if ($option['type'] == 'image') { ?>
                                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-bottom: 5px">
                                              <div class="row">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                      <div style='float:left;padding-left:5px;'>
                                                        <label class="pogonazhLabel<?= $option_value['option_value_id']; ?>">
                                                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" style='display:none;'/>
                                                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  style='height:25px;' /> 
                                                        </label>
                                                      </div>
                                                      <?php } ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <?php } ?>
                                      <?php } ?>
                                  <?php } ?>
                              </div>
                              <?php foreach($pagonazh['attribute_groups'] as $product_attribute){
                                  foreach($product_attribute['attribute'] as $key => $attrib){ ?>
                                      <div class="col-sm-2"><?= $attrib['text']; ?></div>
                                  <?php  };
                              }; ?>
                              <input type="hidden" name="product_id" value="<?= $pagonazh['product_id'];?>">
                              <div class="col-sm-2">
                                  <div class="input-group parent_block_cart">
                                      <span class="input-group-btn">
                                          <button class="dec-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                      </span>
                                      <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                      <span class="input-group-btn">
                                          <button class="inc-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                      </span>
                                  </div>
                              </div>
                              <div class="col-sm-1">
                                  <?php if ($pagonazh['price']) { ?>
                                      <p class="price">
                                        <?php if (!$pagonazh['special']) { ?>
                                        <?php echo $pagonazh['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-new"><?php echo $pagonazh['special']; ?></span> <span class="price-old"><?php echo $pagonazh['price']; ?></span>
                                        <?php } ?>
                                      </p>
                                  <?php } ?>
                              </div>
                              <div class="col-sm-1">
                                  <div class="pogonash_icon_cart" style="height:30px;width: 30px;cursor:pointer;" onclick="services_cart.add('<?php echo $pagonazh['product_id']; ?>');"></div>
                              </div>
                          </div>


                          <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="padding:0px;border-bottom: 1px solid #e2e2e2;margin-top: 20px;padding-bottom: 10px" id='product_ser_<?= $pagonazh["product_id"];?>'>
                              <div class="col-xs-5">
                                  <div class="image">
                                      <a href="<?php echo $pagonazh['href']; ?>">
                                          <img src="<?php echo $pagonazh['thumb']; ?>" alt="<?php echo $pagonazh['name']; ?>" title="<?php echo $pagonazh['name']; ?>" class="img-responsive" style="margin:0 auto;" />
                                      </a>
                                  </div>
                                  <div class="input-group parent_block_cart" style="width:50px;margin:0 auto;">
                                      <span class="input-group-btn">
                                          <button class="dec-c-quantityP btn btn-secondary" type="button" style="padding:6px;background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                      </span>
                                      <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="padding:2px;text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                      <span class="input-group-btn">
                                          <button class="inc-c-quantityP btn btn-secondary" type="button" style="padding:4px;background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                      </span>
                                  </div>
                              </div>
                              <div class="col-xs-5">
                                  <b><?php echo $pagonazh['name']; ?></b>
                                  <?php foreach($pagonazh['attribute_groups'] as $product_attribute){
                                      foreach($product_attribute['attribute'] as $key => $attrib){ ?>
                                          <?= $attrib['text']; ?>
                                      <?php  };
                                  }; ?>

                              <?php if ($pagonazh['price']) { ?>
                                      <p class="price">
                                        <?php if (!$pagonazh['special']) { ?>
                                        <?php echo $pagonazh['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-new"><?php echo $pagonazh['special']; ?></span> <span class="price-old"><?php echo $pagonazh['price']; ?></span>
                                        <?php } ?>
                                      </p>
                                  <?php } ?>
                              </div>
                              <input type="hidden" name="product_id" value="<?= $pagonazh['product_id'];?>">
                              <div class="col-xs-1">
                                  <?php if ($pagonazh['options']) { ?>
                                      <?php foreach ($pagonazh['options'] as $option) { ?>
                                          <?php if ($option['type'] == 'image') { ?>
                                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-bottom: 5px">
                                              <div class="row">
                                                <!--<label class="control-label"><?php echo $option['name']; ?></label>-->
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                      <div style='float:left;padding-left:5px;'>
                                                        <label>
                                                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" style='display:none;'/>
                                                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  style='height:25px;' /> 
                                                        </label>
                                                      </div>
                                                      <?php } ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <?php } ?>
                                      <?php } ?>
                                  <?php } ?>
                                  <div class="pogonash_icon_cart" style="margin-left:-9px;height:30px;width: 30px;cursor:pointer;" onclick="services_cart.add('<?php echo $pagonazh['product_id']; ?>');"></div>
                              </div>
                          </div>
                        <?php } ?>
                      <?php } ?>
                    </div>
                    </div>
            
                    <div class='row'>
                        <div class='col-sm-12' style='padding-top:15px;padding-bottom:15px;'>
                            <div style='float:left;'>
                            <span style='border-bottom:2px dashed #fab446;font-size: 15px'><b>Погонажные изделия</b></span>
                            </div>
                            <div class='icon_arrow_pogonazh' style='float:left;height:20px;width: 20px;margin-left: 5px'></div>
                        </div>
                        <div class='col-sm-12'>
                            <div class="hidden-xs col-sm-12" style="background-color: #f4f2f2;font-size: 12px;padding-top:5px;padding-bottom:5px;margin-top: 5px;margin-bottom: 5px">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">Наименование работ</div>
                                <div class="col-sm-1">Цвет</div>
                                <div class="col-sm-2">Размеры,мм</div>
                                <div class="col-sm-2">Единицы измерения</div>
                                <div class="col-sm-2">Количество</div>
                                <div class="col-sm-1">Цена</div>
                                <div class="col-sm-1"></div>
                            </div>
                      <?php foreach ($pagonazhs as $pagonazh) { ?>
                        <?php if($pagonazh['category_id'] == '69'){ ?>
                        <div class="col-sm-12 hidden-xs pogonazhProduct" style="border-bottom: 1px solid #e2e2e2;margin-top: 20px;padding-bottom: 10px" id='product_ser_<?= $pagonazh["product_id"];?>'>
                              <div class="col-sm-1">
                                  <div class="image">
                                      <a href="<?php echo $pagonazh['href']; ?>">
                                          <img src="<?php echo $pagonazh['thumb']; ?>" alt="<?php echo $pagonazh['name']; ?>" title="<?php echo $pagonazh['name']; ?>" class="img-responsive" />
                                      </a>
                                  </div>
                              </div>

                              <div class="col-sm-2">
                                  <?php echo $pagonazh['name']; ?>
                              </div>
                              <div class="col-sm-1">
                                  <?php if ($pagonazh['options']) { ?>
                                      <?php foreach ($pagonazh['options'] as $option) { ?>
                                          <?php if ($option['type'] == 'image') { ?>
                                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-bottom: 5px">
                                              <div class="row">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                      <div style='float:left;padding-left:5px;'>
                                                        <label class="pogonazhLabel<?= $option_value['option_value_id']; ?>">
                                                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" style='display:none;'/>
                                                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  style='height:25px;' /> 
                                                        </label>
                                                      </div>
                                                      <?php } ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <?php } ?>
                                      <?php } ?>
                                  <?php } ?>
                              </div>
                              <?php foreach($pagonazh['attribute_groups'] as $product_attribute){
                                  foreach($product_attribute['attribute'] as $key => $attrib){ ?>
                                      <div class="col-sm-2"><?= $attrib['text']; ?></div>
                                  <?php  };
                              }; ?>
                              <input type="hidden" name="product_id" value="<?= $pagonazh['product_id'];?>">
                              <div class="col-sm-2">
                                  <div class="input-group parent_block_cart">
                                      <span class="input-group-btn">
                                          <button class="dec-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                      </span>
                                      <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                      <span class="input-group-btn">
                                          <button class="inc-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                      </span>
                                  </div>
                              </div>
                              <div class="col-sm-1">
                                  <?php if ($pagonazh['price']) { ?>
                                      <p class="price">
                                        <?php if (!$pagonazh['special']) { ?>
                                        <?php echo $pagonazh['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-new"><?php echo $pagonazh['special']; ?></span> <span class="price-old"><?php echo $pagonazh['price']; ?></span>
                                        <?php } ?>
                                      </p>
                                  <?php } ?>
                              </div>
                              <div class="col-sm-1">
                                  <div class="pogonash_icon_cart" style="height:30px;width: 30px;cursor:pointer;" onclick="services_cart.add('<?php echo $pagonazh['product_id']; ?>');"></div>
                              </div>
                          </div>


                          <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="padding:0px;border-bottom: 1px solid #e2e2e2;margin-top: 20px;padding-bottom: 10px" id='product_ser_<?= $pagonazh["product_id"];?>'>
                              <div class="col-xs-5">
                                  <div class="image">
                                      <a href="<?php echo $pagonazh['href']; ?>">
                                          <img src="<?php echo $pagonazh['thumb']; ?>" alt="<?php echo $pagonazh['name']; ?>" title="<?php echo $pagonazh['name']; ?>" class="img-responsive" style="margin:0 auto;" />
                                      </a>
                                  </div>
                                  <div class="input-group parent_block_cart" style="width:50px;margin:0 auto;">
                                      <span class="input-group-btn">
                                          <button class="dec-c-quantityP btn btn-secondary" type="button" style="padding:6px;background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                      </span>
                                      <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="padding:2px;text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                      <span class="input-group-btn">
                                          <button class="inc-c-quantityP btn btn-secondary" type="button" style="padding:4px;background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                      </span>
                                  </div>
                              </div>
                              <div class="col-xs-5">
                                  <b><?php echo $pagonazh['name']; ?></b>
                                  <?php foreach($pagonazh['attribute_groups'] as $product_attribute){
                                      foreach($product_attribute['attribute'] as $key => $attrib){ ?>
                                          <?= $attrib['text']; ?>
                                      <?php  };
                                  }; ?>

                              <?php if ($pagonazh['price']) { ?>
                                      <p class="price">
                                        <?php if (!$pagonazh['special']) { ?>
                                        <?php echo $pagonazh['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-new"><?php echo $pagonazh['special']; ?></span> <span class="price-old"><?php echo $pagonazh['price']; ?></span>
                                        <?php } ?>
                                      </p>
                                  <?php } ?>
                              </div>
                              <input type="hidden" name="product_id" value="<?= $pagonazh['product_id'];?>">
                              <div class="col-xs-1">
                                  <?php if ($pagonazh['options']) { ?>
                                      <?php foreach ($pagonazh['options'] as $option) { ?>
                                          <?php if ($option['type'] == 'image') { ?>
                                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-bottom: 5px">
                                              <div class="row">
                                                <!--<label class="control-label"><?php echo $option['name']; ?></label>-->
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                      <div style='float:left;padding-left:5px;'>
                                                        <label>
                                                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="inputColor" style='display:none;'/>
                                                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  style='height:25px;' /> 
                                                        </label>
                                                      </div>
                                                      <?php } ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <?php } ?>
                                      <?php } ?>
                                  <?php } ?>
                                  <div class="pogonash_icon_cart" style="margin-left:-9px;height:30px;width: 30px;cursor:pointer;" onclick="services_cart.add('<?php echo $pagonazh['product_id']; ?>');"></div>
                              </div>
                          </div>
                        <?php } ?>
                      <?php } ?>
                        </div>
                        </div>
            <?php } ?>
            </div>
      
            
            <?php if ($products) { ?>
                    <div class="tab-pane" id="tab-products">
                      <?php $i = 0; ?>
                      <?php foreach ($products as $product) { ?>
                        <div class='col-sm-3 col-md-3 col-lg-2'>
                            <div class="product-thumb transition" style='padding:10px;'>
                                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                    <div class="caption" style='min-height: 50px;padding:0px;'>
                                        <div class="col-sm-12" style="padding:0px;">
                                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row" style="margin-bottom: 5px">
                                                <div class="col-sm-6 col-xs-6" style="padding:0px;">
                                                    <?php if ($product['price']) { ?>
                                                    <p class="price">
                                                      <?php if (!$product['special']) { ?>
                                                        <b><?php echo $product['price']; ?></b>
                                                      <?php } else { ?>
                                                      <b><span class="price-old" style="margin-left: 0px"><?php echo $product['price']; ?></span>
                                                      <span class="price-new"><?php echo $product['special']; ?></span> </b>
                                                      <?php } ?>
                                                    </p>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-6 col-xs-6" style="padding:0px;">
                                                    <div class="input-group parent_block_cart">
                                                        <span class="input-group-btn">
                                                            <button class="dec-c-quantityP btn btn-secondary" type="button" style="padding:8px;background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                                        </span>
                                                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" style="padding-top: 5px;text-align:center;width: 100% !important;text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                                        <span class="input-group-btn">
                                                            <button class="inc-c-quantityP btn btn-secondary" type="button" style="padding:8px;background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-sm-12" style="padding:0px;">
                                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" style='width: 100%;background-color: #f15923;border: 0px solid transparent;color: white;padding:5px;'>В корзину</button>
                                </div>
                            </div>
                        </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
            
            
            <?php if ($services) { ?>
                <div class="tab-pane" id="tab-services">
                    <div class="hidden-xs col-sm-12" style="background-color: #f4f2f2;font-size: 12px;padding-top:5px;padding-bottom:5px;margin-top: 5px;margin-bottom: 5px">
                        <div class="col-sm-2">Наименование работ</div>
                        <div class="col-sm-3">Примечание</div>
                        <div class="col-sm-2">Единицы измерения</div>
                        <div class="col-sm-2">Количество</div>
                        <div class="col-sm-2">Цена</div>
                        <div class="col-sm-1"></div>
                    </div>
                    <?php arsort($services); ?>
                    <?php foreach ($services as $service) { ?>
                            <?php $attrEden = ''; ?>
                            <?php $attrPrem = ''; ?>
                            <?php foreach($service['attribute_groups'] as $product_attribute){
                                foreach($product_attribute['attribute'] as $key => $attrib){
                                    if($key == 0){
                                        $attrEden = $attrib['text'];
                                    }elseif($key == 1){
                                        $attrPrem = $attrib['text'];
                                    }
                                };
                            }; ?>
                      <div class="col-sm-12" style="border-bottom: 1px solid #e2e2e2;margin-bottom: 20px" id='product_ser_<?= $service["product_id"];?>'>
                            <div class="row" style="padding-bottom: 10px">
                                <div class="col-sm-2"><b><?php echo $service['name']; ?></b></div>
                                <div class="col-sm-3">
                                    <?php echo $attrPrem; ?>
                                    <form class="serviceForm<?= $service['product_id'];?>">
                                        <?php if ($service['options']) { ?>
                                            <?php foreach ($service['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'text') { ?>
                                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </form>
                                </div>
                                <div class="col-sm-2"><?php echo $attrEden; ?></div>
                                <input type="hidden" name="product_id" value="<?= $service['product_id'];?>">
                                <div class="col-xs-12 hidden-lg hidden-md hidden-sm"></div>
                                <div class="col-sm-2 col-xs-5">
                                    <?php if (!$service['options']) { ?>
                                    <div class="input-group parent_block_cart">
                                        <span class="input-group-btn">
                                            <button class="dec-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                        </span>
                                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                        <span class="input-group-btn">
                                            <button class="inc-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                        </span>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                <?php if ($service['price']) { ?>
                                    <p class="price" style="margin:0 auto;">
                                      <?php if (!$service['special']) { ?>
                                        <b><?php echo $service['price']; ?></b>
                                      <?php } else { ?>
                                      <span class="price-new"><?php echo $service['special']; ?></span> <span class="price-old"><?php echo $service['price']; ?></span>
                                      <?php } ?>
                                    </p>
                                <?php } ?>
                                </div>
                                <div class="col-sm-1 col-xs-3">
                                    <div class="pogonash_icon_cart" data-option_id="<?= $service['product_id'];?>" style="height:30px;width: 30px;cursor:pointer;" onclick="services_cart.add('<?php echo $service['product_id']; ?>');"></div>
                                    <!--<button type="button" > <i class="fa fa-shopping-cart"></i></button>-->
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                        Весь комплекс услуг <a style='cursor:pointer' id="btnExport">в Прайс - листе <i class='fa fa-long-arrow-down'></i></a>
                        <table id="tblExport" style="display:none;">
                            <tr>
                                <td><b>Наименование работ</b></td>
                                <td><b>Примечание</b></td>
                                <td><b>Единицы измерения</b></td>
                                <td><b>Цена</b></td>
                            </tr>
                            <?php $attrEden = ''; ?>
                            <?php $attrPrem = ''; ?>
                            <?php foreach($service['attribute_groups'] as $product_attribute){
                                foreach($product_attribute['attribute'] as $key => $attrib){
                                    if($key == 0){
                                        $attrEden = $attrib['text'];
                                    }elseif($key == 1){
                                        $attrPrem = $attrib['text'];
                                    }
                                };
                            }; ?>
                            <?php foreach ($services as $service) { ?>
                                <tr>
                                    <td><?= $service['name']; ?></td>
                                    <td><?= $attrPrem; ?></td>
                                    <td><?= $attrEden; ?></td>
                                    <td>
                                        <?php if ($service['price']) { ?>
                                          <?php if (!$service['special']) { ?>
                                            <?php echo $service['price']; ?>
                                          <?php } else { ?>
                                            <?php echo $service['special']; ?>
                                          <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                </div>
            <?php } ?>
      
      
            
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
          </div>                               
    </div>
    <div class="row" style='margin-top: 20px'>
        <?php if ($review_status) { ?>
          <div class="rating">
            
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
            <!-- AddThis Button END -->
          </div>
          <?php } ?>
    </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$(document).on('click','#button-cart', function() {
    
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
</script>

				<script type="text/javascript"><!--
				$(document).ready(function() {
					$('select[name^=\'option\']').trigger('change');
				});
				
				$(document).on('change', 'select[name^=\'option\']', function() {
					$(this).after('<i class="fa fa-spinner selection-wait"></i>');
					
					var value = $(this).val();
					var parent_id = $(this).attr('name').replace (/[^\d.]/g, '');

					var select_obj = $(this);
				
					$.ajax({
						url: 'index.php?route=product/product/dependentoption&parent_id=' +  parent_id + '&value=' + value + '&product_id=<?php echo $product_id; ?>',
						type: 'get',
						dataType: 'json',			
						success: function(json) {
							$('.selection-wait').remove();

							 if(select_obj.children('option:selected').attr('data-hint') != ''){
								select_obj.next().html(select_obj.children('option:selected').attr('data-hint'));

							}
							
							if (json['option']) {
								for (i = 0; i < json['option'].length; i++) {
									if (json['option'][i]['type'] == 'select') {
										$('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');
										
										$('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');
										
										var html = '';
									
										html += '<option value=""><?php echo $text_select; ?></option>';
											
										for (j = 0; j < json['option'][i]['option_value'].length; j++) {
											$('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');
											
											$('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');
											
											html += '<option value="' + json['option'][i]['option_value'][j]['product_option_value_id'] + '">' + json['option'][i]['option_value'][j]['name'];
											
											if (json['option'][i]['option_value'][j]['price']) {
												html += ' (' + json['option'][i]['option_value'][j]['price_prefix'] + json['option'][i]['option_value'][j]['price'] + ')';
											}
											
											html += '</option>';
										}
										
										$('select[name=\'option[' + json['option'][i]['product_option_id'] + ']\']').html(html);
									} else if (json['option'][i]['type'] == 'radio' || json['option'][i]['type'] == 'checkbox' || json['option'][i]['type'] == 'image') {
										$('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');
										
										$('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');
										
										$('#input-option' + json['option'][i]['product_option_id']).children().hide();
										
										$('#input-option' + json['option'][i]['product_option_id']).find('input').prop('checked', false);
									
										for (j = 0; j < json['option'][i]['option_value'].length; j++) {
											$('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');
											
											$('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');
																						
											$('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().show();
											
											$('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().parent().show();
										}
									} else {
										// File, text, textarea, date, datetime
										if (json['option'][i]['option_value']) {
											$('#input-option' + json['option'][i]['product_option_id']).parents('.form-group').stop().fadeIn('medium');
										} else {
											$('#input-option' + json['option'][i]['product_option_id']).parents('.form-group').stop().fadeOut('medium');
										}
									}
								}
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});	
				});
				//--></script>
			
<?php echo $footer; ?>
