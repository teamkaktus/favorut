<?php  
class ControllerModuleNews extends Controller {
	public function index() {
		$this->language->load('module/news');
		$this->load->model('extension/news');
		$this->load->model('tool/image');
		
		$data['heading_title'] = $this->language->get('heading_title');
	 
		$all_news = $this->model_extension_news->getAllHomeNews(['page' => 1,'start' => 0,'type' => 1,'limit' => 3]);
		$all_video = $this->model_extension_news->getAllHomeNews(['page' => 1,'start' => 0,'type' => 3,'limit' => 2]);
		$all_statia = $this->model_extension_news->getAllHomeNews(['page' => 1,'start' => 0,'type' => 2,'limit' => 2]);
                
                $data['all_news_link'] = $this->url->link('information/news');
                
                $data['video'] = [];
                $data['newsS'] = [];
                $data['statia'] = [];
                    foreach ($all_news as $news) {
                                $data['newsS'][] = array (
                                    'title' 		=> html_entity_decode($news['title'], ENT_QUOTES),
                                    'news_type' 		=> $news['news_type'],
                                    'description' 	=> (strlen(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))) > 150 ? substr(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES)), 0, 150) . '...' : strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))),
                                    'view' 			=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
                                    'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
                                );
                    }
                    foreach ($all_video as $news) {
                                $data['video'][] = array (
                                    'title' 		=> html_entity_decode($news['title'], ENT_QUOTES),
                                    'news_type' 	=> $news['news_type'],
                                    'image' 		=> $this->model_tool_image->resize($news['image'], 500, 500),
                                    'description' 	=> (strlen(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))) > 150 ? substr(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES)), 0, 150) . '...' : strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))),
                                    'view' 		=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
                                    'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
                                );
                    }
                    
                    foreach ($all_statia as $news) {
                                $data['statia'][] = array (
                                    'title' 		=> html_entity_decode($news['title'], ENT_QUOTES),
                                    'news_type' 	=> $news['news_type'],
                                    'image' 		=> $this->model_tool_image->resize($news['image'], 500, 500),
                                    'description' 	=> (strlen(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))) > 150 ? substr(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES)), 0, 150) . '...' : strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))),
                                    'view' 		=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
                                    'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
                                );
                    }
                    
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/news.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/news.tpl', $data);
		} else {
			return $this->load->view('default/template/module/news.tpl', $data);
		}
	}
}