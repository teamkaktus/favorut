<?php  
class ControllerModuleShippingMoscow extends Controller {
	public function moscow_cost() {
		$this->load->language('shipping/moscow');
		$weight = floatval($this->request->get['weight']);
		$city = floatval($this->request->get['city']);
		$this->load->model('shipping/moscow');
		$show_weight = false;

		$cost = $this->model_shipping_moscow->getMoscowCost($weight, $show_weight, $city);
		unset($this->session->data['session_city']);
		unset($this->session->data['session_address']);
			
		$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('moscow_tax_class_id'), $this->config->get('config_tax')));
		
		$ret = array(
				'code'         => 'moscow.moscow',
				'title'        => $this->model_shipping_moscow->getMoscowTitle($weight, $show_weight),
				'cost'         => $cost,
				'tax_class_id' => $this->config->get('moscow_tax_class_id'),
				'note'		   => '(При заказе от 50000руб.Доставка по Москве и МО 30км от  МКАД бесплатно)',
				'text'         => $text);
		if (isset($this->session->data['shipping_methods']['moscow']) && isset($this->session->data['shipping_methods']['moscow']['quote'])) {
			$this->session->data['shipping_methods']['moscow']['quote']['moscow'] = $ret;
		}
		
		$this->response->setOutput(json_encode($ret));
	}

	public function zamkad_cost() {
		$this->load->language('shipping/moscow');
		$weight = floatval($this->request->get['weight']);
		$city = trim($this->request->get['city']);
		$distance = floatval($this->request->get['zamkad_distance']);

        unset($this->session->data['moscow_city']);
		unset($this->session->data['session_city']);
		unset($this->session->data['session_address']);

		if ($distance > 0) {
			$this->load->model('shipping/moscow');

			$show_weight = false;
			$cost = $this->model_shipping_moscow->getZamkadCost($weight, $distance, $show_weight, $city);

            if ($cost == 0) {
                $ret = array(
                    'code'         => 'moscow.moscow',
                    'title'        => sprintf($this->language->get('free_shipping'), $city, (float)$distance),
                    'cost'         => 0,
                    'note'         => '',
                    'distance'	   => $distance,
                    'tax_class_id' => $this->config->get('moscow_tax_class_id'),
                    'text'         => $this->currency->format($cost)
                );
            } else if ($cost == 'toofar') {
				$ret = array(
						'code'         => 'moscow.moscow',
						'title'        => sprintf($this->language->get('zamkad_toofar'), (int)$this->config->get('moscow_max_distance')),
						'cost'         => 0,
						'distance'	   => 'error',
						'tax_class_id' => $this->config->get('moscow_tax_class_id'),
						'text'         => ''
				);
			} else {
				$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('moscow_tax_class_id'), $this->config->get('config_tax')));
				
				$ret = array(
						'code'         => 'moscow.moscow',
						'title'        => $this->model_shipping_moscow->getZamkadTitle($weight, $distance, $show_weight),
						'cost'         => $cost,
						'distance'	   => $distance,
						'note'		   => '(При заказе от 50000руб.Доставка по Москве и МО 30км от  МКАД бесплатно)',
						'tax_class_id' => $this->config->get('moscow_tax_class_id'),
						'text'         => $text);
			}
			
			if (isset($this->session->data['shipping_methods']['moscow']) && isset($this->session->data['shipping_methods']['moscow']['quote'])) {
				$this->session->data['shipping_methods']['moscow']['quote']['moscow'] = $ret;
			}
			
			$this->response->setOutput(json_encode($ret));
		}
	}
	
	public function zamkad_fault() {
		$this->load->language('shipping/moscow');
		$this->load->model('shipping/moscow');
		
		$ret = array(
				'code'         => 'moscow.moscow',
				'title'        => $this->model_shipping_moscow->getZamkadErrTitle(),
				'cost'         => 0,
				'distance'	   => 'error',
				'tax_class_id' => $this->config->get('moscow_tax_class_id'),
				'text'         => '');
						
		if (isset($this->session->data['shipping_methods']['moscow']) && isset($this->session->data['shipping_methods']['moscow']['quote'])) {
			$this->session->data['shipping_methods']['moscow']['quote']['moscow'] = $ret;
		}
		$this->response->setOutput(json_encode($ret));
	}
	public function setSession_address(){
		$city = trim($this->request->post['session_city']);
		$address = trim($this->request->post['session_address']);
		$this->session->data['session_city'] = $city;
		$this->session->data['session_address'] = $address;
	}
	public function clearSession_address(){
		unset($this->session->data['session_city']);
		unset($this->session->data['session_address']);
	}
}
?>
