<?php

class ControllerAccountAccount extends Controller
{
    private $error = array();

    private $as_error = array();

    public function index()
    {
        $this->document->addStyle('catalog/view/theme/default/stylesheet/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/owl.carousel.js');
        //$this->document->addScript('catalog/view/javascript/owl.carousel.min.js');
        
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->load->language('account/account');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }


        $data['heading_title'] = $this->language->get('heading_title');

        $data['tab_profile'] = $this->language->get('text_my_profile');
        $data['tab_purchase_history'] = $this->language->get('text_my_purchase_history');
        $data['tab_wish_list'] = $this->language->get('text_my_wish_list');
        $data['tab_comparison_list'] = $this->language->get('text_my_comparison_list');
        $data['tab_cart'] = $this->language->get('text_my_cart');

        if ($this->config->get('reward_status')) {
            $data['reward'] = $this->url->link('account/reward', '', 'SSL');
        } else {
            $data['reward'] = '';
        }

        $data['cart_url'] = $this->url->link('checkout/cart');

        // ************* PROFIL *********************

        if (isset($this->session->data['pr_success'])) {
            $data['pr_success'] = $this->session->data['pr_success'];

            unset($this->session->data['pr_success']);
        } else {
            $data['pr_success'] = '';
        }

        $data['pr_heading_title'] = $this->language->get('heading_title');

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript('catalog/view/javascript/account.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->model('account/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['action_profile']) && $this->profile_validate()) {
            $this->model_account_customer->editCustomer($this->request->post);

            $this->session->data['pr_success'] = $this->language->get('pr_text_success');

            // Add to activity log
            $this->load->model('account/activity');

            $activity_data = array(
                'customer_id' => $this->customer->getId(),
                'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
            );

            $this->model_account_activity->addActivity('edit', $activity_data);

            $this->response->redirect($this->url->link('account/account', '', 'SSL') . '#profile');
        }

        $data['pr_text_your_details'] = $this->language->get('pr_text_your_details');
        $data['text_select'] = $this->language->get('text_select');
        $data['button_change'] = $this->language->get('button_change');

        $data['pr_entry_firstname'] = $this->language->get('pr_entry_firstname');
        $data['pr_entry_lastname'] = $this->language->get('pr_entry_lastname');
        $data['pr_entry_email'] = $this->language->get('pr_entry_email');
        $data['pr_entry_nicname'] = $this->language->get('pr_entry_nicname');
        $data['pr_entry_telephone'] = $this->language->get('pr_entry_telephone');
        $data['pr_entry_sex'] = $this->language->get('pr_entry_sex');
        $data['pr_entry_birthday'] = $this->language->get('pr_entry_birthday');

        if (isset($this->error['warning'])) {
            $data['pr_error_warning'] = $this->error['pr_warning'];
        } else {
            $data['pr_error_warning'] = '';
        }

        if (isset($this->error['pr_firstname'])) {
            $data['pr_error_firstname'] = $this->error['pr_firstname'];
        } else {
            $data['pr_error_firstname'] = '';
        }

        if (isset($this->error['pr_lastname'])) {
            $data['pr_error_lastname'] = $this->error['pr_lastname'];
        } else {
            $data['pr_error_lastname'] = '';
        }

        if (isset($this->error['pr_email'])) {
            $data['pr_error_email'] = $this->error['pr_email'];
        } else {
            $data['pr_error_email'] = '';
        }

        if (isset($this->error['pr_telephone'])) {
            $data['pr_error_telephone'] = $this->error['pr_telephone'];
        } else {
            $data['pr_error_telephone'] = '';
        }

        if (isset($this->error['pr_custom_field'])) {
            $data['pr_error_custom_field'] = $this->error['pr_custom_field'];
        } else {
            $data['pr_error_custom_field'] = array();
        }


        $data['ac_action'] = $this->url->link('account/account', '', 'SSL');
        if ($this->request->server['REQUEST_METHOD'] != 'POST' || !isset($this->request->post['action_profile'])) {
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
        }

        if (isset($this->request->post['action_profile']) && isset($this->request->post['firstname'])) {
            $data['pr_firstname'] = $this->request->post['firstname'];
        } elseif (!empty($customer_info)) {
            $data['pr_firstname'] = $customer_info['firstname'];
        } else {
            $data['pr_firstname'] = '';
        }

        if (isset($this->request->post['action_profile']) && isset($this->request->post['lastname'])) {
            $data['pr_lastname'] = $this->request->post['lastname'];
        } elseif (!empty($customer_info)) {
            $data['pr_lastname'] = $customer_info['lastname'];
        } else {
            $data['pr_lastname'] = '';
        }

        if (isset($this->request->post['action_profile']) && isset($this->request->post['email'])) {
            $data['pr_email'] = $this->request->post['email'];
        } elseif (!empty($customer_info)) {
            $data['pr_email'] = $customer_info['email'];
        } else {
            $data['pr_email'] = '';
        }

        if (isset($this->request->post['action_profile']) && isset($this->request->post['telephone'])) {
            $data['pr_telephone'] = $this->request->post['telephone'];
        } elseif (!empty($customer_info)) {
            $data['pr_telephone'] = $customer_info['telephone'];
        } else {
            $data['pr_telephone'] = '';
        }

        if (isset($this->request->post['action_profile']) && isset($this->request->post['fax'])) {
            $data['fax'] = $this->request->post['fax'];
        } elseif (!empty($customer_info)) {
            $data['fax'] = $customer_info['fax'];
        } else {
            $data['fax'] = '';
        }

        // Custom Fields
        $this->load->model('account/custom_field');

        $data['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

        if (isset($this->request->post['custom_field'])) {
            $data['account_custom_field'] = $this->request->post['custom_field'];
        } elseif (isset($customer_info)) {
            $data['account_custom_field'] = json_decode($customer_info['custom_field'], true);
        } else {
            $data['account_custom_field'] = array();
        }
        // ************* /PROFIL ********************

        // ************* PASSWORD ********************

        if (isset($this->session->data['ps_success'])) {
            $data['ps_success'] = $this->session->data['ps_success'];

            unset($this->session->data['ps_success']);
        } else {
            $data['ps_success'] = '';
        }

        $data['ps_text_your_password'] = $this->language->get('ps_text_your_password');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['action_password']) && $this->password_validate()) {
            $this->load->model('account/customer');

            $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);

            $this->session->data['ps_success'] = $this->language->get('ps_text_success');

            // Add to activity log
            $this->load->model('account/activity');

            $activity_data = array(
                'customer_id' => $this->customer->getId(),
                'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
            );

            $this->model_account_activity->addActivity('password', $activity_data);

            $this->response->redirect($this->url->link('account/account', '', 'SSL') . '#profile');
        }

        $data['ps_entry_password'] = $this->language->get('ps_entry_password');
        $data['ps_entry_confirm'] = $this->language->get('ps_entry_confirm');


        if (isset($this->error['ps_password'])) {
            $data['ps_error_password'] = $this->error['ps_password'];
        } else {
            $data['ps_error_password'] = '';
        }

        if (isset($this->error['ps_confirm'])) {
            $data['ps_error_confirm'] = $this->error['ps_confirm'];
        } else {
            $data['ps_error_confirm'] = '';
        }

        if (isset($this->request->post['password'])) {
            $data['ps_password'] = $this->request->post['password'];
        } else {
            $data['ps_password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $data['ps_confirm'] = $this->request->post['confirm'];
        } else {
            $data['ps_confirm'] = '';
        }
        // ************* /PASSWORD *******************


        // ************* ADDRESS *********************

        // ------------- GET ADD FORM ----------------
        $this->load->model('account/address');

        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['as_entry_firstname'] = $this->language->get('as_entry_firstname');
        $data['as_entry_lastname'] = $this->language->get('as_entry_lastname');
        $data['as_entry_company'] = $this->language->get('as_entry_company');
        $data['as_entry_address_1'] = $this->language->get('as_entry_address_1');
        $data['as_entry_address_2'] = $this->language->get('as_entry_address_2');
        $data['as_entry_postcode'] = $this->language->get('as_entry_postcode');
        $data['as_entry_city'] = $this->language->get('as_entry_city');
        $data['as_entry_country'] = $this->language->get('as_entry_country');
        $data['as_entry_zone'] = $this->language->get('as_entry_zone');
        $data['as_entry_default'] = $this->language->get('as_entry_default');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_back'] = $this->language->get('button_back');
        $data['button_upload'] = $this->language->get('button_upload');
        $data['as_button_addAddress'] = $this->language->get('as_button_addAddress');

        if (isset($this->request->post['addAddress']) && ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateAddressForm()) {
            $this->model_account_address->addAddress($this->request->post);


            $this->session->data['as_success'] = $this->language->get('as_text_add');

            // Add to activity log
            $this->load->model('account/activity');

            $activity_data = array(
                'customer_id' => $this->customer->getId(),
                'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
            );

            $this->model_account_activity->addActivity('address_add', $activity_data);

            $this->response->redirect($this->url->link('account/account#profil', '', 'SSL'));
        }

        if (isset($this->as_error['as_firstname'])) {
            $data['as_error_firstname'] = $this->as_error['as_firstname'];
        } else {
            $data['as_error_firstname'] = '';
        }

        if (isset($this->as_error['as_lastname'])) {
            $data['as_error_lastname'] = $this->as_error['as_lastname'];
        } else {
            $data['as_error_lastname'] = '';
        }

        if (isset($this->as_error['as_address_1'])) {
            $data['as_error_address_1'] = $this->as_error['as_address_1'];
        } else {
            $data['as_error_address_1'] = '';
        }

        if (isset($this->as_error['as_city'])) {
            $data['as_error_city'] = $this->as_error['as_city'];
        } else {
            $data['as_error_city'] = '';
        }


        if (isset($this->as_error['as_postcode'])) {
            $data['as_error_postcode'] = $this->as_error['as_postcode'];
        } else {
            $data['as_error_postcode'] = '';
        }

        if (isset($this->as_error['as_country'])) {
            $data['as_error_country'] = $this->as_error['as_country'];
        } else {
            $data['as_error_country'] = '';
        }

        if (isset($this->as_error['as_zone'])) {
            $data['as_error_zone'] = $this->as_error['as_zone'];
        } else {
            $data['as_error_zone'] = '';
        }

        if (isset($this->as_error['as_custom_field'])) {
            $data['as_error_custom_field'] = $this->as_error['as_custom_field'];
        } else {
            $data['as_error_custom_field'] = array();
        }

        /*if (!isset($this->request->get['address_id'])) {*/
        $data['as_action'] = $this->url->link('account/account', '', 'SSL');
        /*} else {
            $data['action'] = $this->url->link('account/address/edit', 'address_id=' . $this->request->get['address_id'], 'SSL');
        }*/

        if (isset($this->request->post['address_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $address_info = $this->model_account_address->getAddress($this->request->get['address_id']);
        }


        if (isset($this->request->post['addAddress']) && isset($this->request->post['firstname'])) {
            $data['as_firstname'] = $this->request->post['firstname'];
        } elseif (!empty($customer_info)) {
            $data['as_firstname'] = $customer_info['firstname'];
        } else {
            $data['as_firstname'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['lastname'])) {
            $data['as_lastname'] = $this->request->post['lastname'];
        } elseif (!empty($customer_info)) {
            $data['as_lastname'] = $customer_info['lastname'];
        } else {
            $data['as_lastname'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['company'])) {
            $data['as_company'] = $this->request->post['company'];
        } elseif (!empty($address_info)) {
            $data['as_company'] = $address_info['company'];
        } else {
            $data['as_company'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['address_1'])) {
            $data['as_address_1'] = $this->request->post['address_1'];
        } elseif (!empty($address_info)) {
            $data['as_address_1'] = $address_info['address_1'];
        } else {
            $data['as_address_1'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['address_2'])) {
            $data['as_address_2'] = $this->request->post['address_2'];
        } elseif (!empty($address_info)) {
            $data['as_address_2'] = $address_info['address_2'];
        } else {
            $data['as_address_2'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['postcode'])) {
            $data['as_postcode'] = $this->request->post['postcode'];
        } elseif (!empty($address_info)) {
            $data['as_postcode'] = $address_info['postcode'];
        } else {
            $data['as_postcode'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['city'])) {
            $data['as_city'] = $this->request->post['city'];
        } elseif (!empty($address_info)) {
            $data['as_city'] = $address_info['city'];
        } else {
            $data['as_city'] = '';
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['country_id'])) {
            $data['country_id'] = $this->request->post['country_id'];
        } elseif (!empty($address_info)) {
            $data['country_id'] = $address_info['country_id'];
        } else {
            $data['country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['addAddress']) && isset($this->request->post['zone_id'])) {
            $data['zone_id'] = $this->request->post['zone_id'];
        } elseif (!empty($address_info)) {
            $data['zone_id'] = $address_info['zone_id'];
        } else {
            $data['zone_id'] = '';
        }

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        // Custom fields
        $this->load->model('account/custom_field');

        $data['as_custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));
        if (isset($this->request->post['custom_field'])) {
            $data['address_custom_field'] = $this->request->post['custom_field'];
        } elseif (isset($address_info)) {
            $data['address_custom_field'] = $address_info['custom_field'];
        } else {
            $data['address_custom_field'] = array();
        }

        if (isset($this->request->post['default'])) {
            $data['default'] = $this->request->post['default'];
        } elseif (isset($this->request->get['address_id'])) {
            $data['default'] = $this->customer->getAddressId() == $this->request->get['address_id'];
        } else {
            $data['default'] = false;
        }


        // ------------- /GET ADD FORM  --------------
        // ------------- GET ADDRESS -----------------

        $data['as_text_address_book'] = $this->language->get('as_text_address_book');
        $data['as_text_empty'] = $this->language->get('as_text_empty');

        $data['as_button_new_address'] = $this->language->get('as_button_new_address');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['as_warning'])) {
            $data['as_error_warning'] = $this->error['as_warning'];
        } else {
            $data['as_error_warning'] = '';
        }


        $data['addresses'] = array();

        $results = $this->model_account_address->getAddresses();

        foreach ($results as $result) {
            if ($result['address_format']) {
                $format = $result['address_format'];
            } else {
                $format = '{city}, ';
            }
            $find = array(
                '{city}'
            );

            $replace = array(
                'city' => 'г.' . $result['city']
            );
            $custom_fields = $result['custom_field'];
            if(!empty($custom_fields)){
                foreach ($custom_fields as $key => $custom_field) {
                    $field_name = $this->model_account_address->getCustomField($key);

                    if (!next($custom_fields)) {
                        $format .= '{' . mb_strtolower($field_name['name']) . '}';

                    } else {
                        $format .= '{' . mb_strtolower($field_name['name']) . '}, ';
                    }

                    array_push($find, '{' . mb_strtolower($field_name['name']) . '}');
                    if ($custom_field !== '') {
                        $replace_tmp = array(
                            mb_strtolower($field_name['name']) => mb_strtolower($field_name['name']) . ' ' . $custom_field
                        );
                        $replace = array_merge($replace, $replace_tmp);
                    }

                }
            }


            $data['addresses'][] = array(
                'address_id' => $result['address_id'],
                'address' => trim(str_replace($find, $replace, $format)),
                'update' => $this->url->link('account/address/edit', 'address_id=' . $result['address_id'], 'SSL'),
                'delete' => $this->url->link('account/account/deleteAddress', 'address_id=' . $result['address_id'] . '&deleteAddress=true', 'SSL')
            );


        }


        if (isset($this->request->get['deleteAddress']) && isset($this->request->get['address_id']) && $this->validateDeleteAddress()) {
            $this->deleteAddress();
        }
        if (isset($this->session->data['as_success'])) {
            $data['as_success'] = $this->session->data['as_success'];

            unset($this->session->data['as_success']);
        } else {
            $data['as_success'] = '';
        }

        // ------------- /GET ADDRESS ----------------
        // ************* /ADDRESS ********************
        
        // ************* ORDER ***********************
        $this->load->model('account/order');
        $this->load->model('catalog/product');

        $this->load->model('tool/image');
        $orders_account = $this->model_account_order->getOrdersAccount();

        $product_atributes = array();
        $data['ph_product_attributes'] = array();

        foreach ($orders_account as $order_account) {
            $product_info = $this->model_catalog_product->getProduct($order_account['product_id']);
            $discounts = $this->model_catalog_product->getProductDiscounts($order_account['product_id']);

            foreach ($discounts as $discount) {
                $res_discounts[] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
                );
            }

            $attribute_groups = $this->model_catalog_product->getProductAttributes($order_account['product_id']);
            foreach ($attribute_groups as $attribute_group) {
                if($attribute_group['attribute_group_id'] == 7) {
                    foreach ($attribute_group['attribute'] as $product_attribute) {
                        if($product_attribute['attribute_id'] == 12) {
                            $product_atributes = array(
                                $order_account['product_id'] => $product_attribute['text']
                            );
                        }
                    }
                }
            }

            array_push($data['ph_product_attributes'], $product_atributes);

            $ph_options = array();

            $order_option_id = array();

            $order_options = $this->model_account_order->getOrderOptions($order_account['order_id'], $order_account['order_product_id']);
            foreach($order_options as $order_option){
                $order_option_id[$order_option['product_option_id']] = array(
                    'value_id' =>  $order_option['product_option_value_id']
                );
            }

           /* echo '<pre> order options <br>';
            var_dump( $order_option_id );
            echo '</pre>';

            echo '<pre> product options <br>';
            var_dump( $this->model_catalog_product->getProductOptions($order_account['product_id']) );
            echo '</pre>';*/

            $res_discounts = array();

            foreach ($this->model_catalog_product->getProductOptions($order_account['product_id']) as $option) {
                $product_option_value_data = array();

                foreach ($option['product_option_value'] as $option_value) {
                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                        } else {
                            $price = false;
                        }

                        if(isset($order_option_id[$option['product_option_id']]['value_id']) && $order_option_id[$option['product_option_id']]['value_id']  == $option_value['product_option_value_id']){

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }
                }

                $ph_options[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $data['ph_products'][] = array(
                'id'         => $order_account['product_id'],
                'image'      => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], 127, 127) : '',
                'name'       => $product_info['name'] ? $product_info['name'] : '',
                'quantity'       => $order_account['quantity'] ? $order_account['quantity'] : 1,
                'price'      => !$this->config->get('config_customer_price') ? $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'special'    => (float)$product_info['special'] ? $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'tax'        => $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']),
                'discounts'  => $res_discounts,
                'options'    => $ph_options,
                'href'        => $this->url->link('product/product', 'product_id=' . $order_account['product_id']),
                'date'       => date( 'd.m.Y', strtotime($order_account['date_added']))
            );



        }

        // ************* /ORDER **********************

        // ************* WISHLIST ********************
        $this->load->model('account/wishlist');

        $this->load->model('catalog/product');

        if (isset($this->request->get['wl_remove'])) {
            // Remove Wishlist
            $this->model_account_wishlist->deleteWishlist($this->request->get['wl_remove']);

            $this->session->data['wl_success'] = $this->language->get('wl_text_remove');

            $this->response->redirect($this->url->link('account/account') . '#wish_list');
        }

        $data['wl_text_empty'] = $this->language->get('wl_text_empty');


        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_remove'] = $this->language->get('button_remove');

        if (isset($this->session->data['wl_success'])) {
            $data['wl_success'] = $this->session->data['wl_success'];
            unset($this->session->data['wl_success']);
        } else {
            $data['wl_success'] = '';
        }

        $data['wl_products'] = array();

        $results = $this->model_account_wishlist->getWishlist();

        foreach ($results as $result) {
            $product_info = $this->model_catalog_product->getProduct($result['product_id']);
            $discounts = $this->model_catalog_product->getProductDiscounts($result['product_id']);

            $res_discounts = array();

            foreach ($discounts as $discount) {
                $res_discounts[] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
                );
            }

            $attribute_groups = $this->model_catalog_product->getProductAttributes($result['product_id']);
            foreach ($attribute_groups as $attribute_group) {
                if($attribute_group['attribute_group_id'] == 7) {
                    foreach ($attribute_group['attribute'] as $product_attribute) {
                        if($product_attribute['attribute_id'] == 12) {
                            $product_atributes = array(
                                $result['product_id'] => $product_attribute['text']
                            );
                        }
                    }
                }
            }
            $data['wl_product_attributes'] = $product_atributes;

            $wl_options = array();

            foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                $wl_product_option_value_data = array();

                foreach ($option['product_option_value'] as $option_value) {
                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                        } else {
                            $price = false;
                        }

                        $wl_product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $wl_options[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $wl_product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $data['wl_products'][] = array(
                'id'         => $result['product_id'],
                'image'      => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], 127, 127) : '',
                'name'       => $product_info['name'] ? $product_info['name'] : '',
                'price'      => !$this->config->get('config_customer_price') ? $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'special'    => (float)$product_info['special'] ? $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'tax'        => $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']),
                'discounts'  => $res_discounts,
                'options'    => $wl_options,
                'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'remove'     => $this->url->link('account/account', 'wl_remove=' . $result['product_id'])
            );
        }
        // ************* /WISHLIST *******************

        // ************* COMPARE *********************

        if (!isset($this->session->data['compare'])) {
            $this->session->data['compare'] = array();
        }

        if (isset($this->request->get['cm_remove'])) {
            $key = array_search($this->request->get['cm_remove'], $this->session->data['compare']);

            if ($key !== false) {
                unset($this->session->data['compare'][$key]);
            }

            $this->session->data['cm_success'] = $this->language->get('cm_text_remove');

            $this->response->redirect($this->url->link('account/account').'#comparison_list');
        }

        $data['cm_text_compare'] = $this->language->get('cm_text_compare');

        $data['cm_text_product'] = $this->language->get('cm_text_product');
        $data['cm_text_name'] = $this->language->get('cm_text_name');
        $data['cm_text_image'] = $this->language->get('cm_text_image');
        $data['cm_text_price'] = $this->language->get('cm_text_price');
        $data['cm_text_model'] = $this->language->get('cm_text_model');
        $data['cm_text_manufacturer'] = $this->language->get('cm_text_manufacturer');
        $data['cm_text_availability'] = $this->language->get('cm_text_availability');
        $data['cm_text_rating'] = $this->language->get('cm_text_rating');
        $data['cm_text_summary'] = $this->language->get('cm_text_summary');
        $data['cm_text_weight'] = $this->language->get('cm_text_weight');
        $data['cm_text_dimension'] = $this->language->get('cm_text_dimension');
        $data['cm_text_empty'] = $this->language->get('cm_text_empty');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_remove'] = $this->language->get('button_remove');


        if (isset($this->session->data['cm_success'])) {
            $data['cm_success'] = $this->session->data['cm_success'];

            unset($this->session->data['cm_success']);
        } else {
            $data['cm_success'] = '';
        }

        $data['review_status'] = $this->config->get('config_review_status');

        $data['cm_products_to_category'] = array();

        $data['cm_attribute_groups'] = array();
        $data['cm_categories'] = array();

        $cm_categories = array();

        foreach ($this->session->data['compare'] as $key => $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_compare_width'), $this->config->get('config_image_compare_height'));
                } else {
                    $image = false;
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($product_info['quantity'] <= 0) {
                    $availability = $product_info['stock_status'];
                } elseif ($this->config->get('config_stock_display')) {
                    $availability = $product_info['quantity'];
                } else {
                    $availability = $this->language->get('text_instock');
                }
                $attribute_data = array();

                $attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);

                foreach ($attribute_groups as $attribute_group) {
                    foreach ($attribute_group['attribute'] as $attribute) {
                        $attribute_data[$attribute['attribute_id']] = $attribute['text'];
                    }
                }

                $tmp_categories = $this->model_catalog_product->getCategories($product_id);
                $category_id = reset($tmp_categories)['category_id'];
                array_push($cm_categories, $category_id);

                $data['cm_products_to_category'][$category_id][$product_id] = array(
                    'product_id'   => $product_info['product_id'],
                    'name'         => $product_info['name'],
                    'thumb'        => $image,
                    'price'        => $price,
                    'special'      => $special,
                    'description'  => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
                    'model'        => $product_info['model'],
                    'manufacturer' => $product_info['manufacturer'],
                    'availability' => $availability,
                    'minimum'      => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                    'rating'       => (int)$product_info['rating'],
                    'reviews'      => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
                    'weight'       => $this->weight->format($product_info['weight'], $product_info['weight_class_id']),
                    'length'       => $this->length->format($product_info['length'], $product_info['length_class_id']),
                    'width'        => $this->length->format($product_info['width'], $product_info['length_class_id']),
                    'height'       => $this->length->format($product_info['height'], $product_info['length_class_id']),
                    'attribute'    => $attribute_data,
                    'href'         => $this->url->link('product/product', 'product_id=' . $product_id),
                    'remove'       => $this->url->link('account/account', 'cm_remove=' . $product_id)
                );


                foreach ($attribute_groups as $attribute_group) {
                    $data['cm_attribute_groups'][$attribute_group['attribute_group_id']]['name'] = $attribute_group['name'];

                    foreach ($attribute_group['attribute'] as $attribute) {
                        $data['cm_attribute_groups'][$attribute_group['attribute_group_id']]['attribute'][$attribute['attribute_id']]['name'] = $attribute['name'];
                    }
                }


            } else {
                unset($this->session->data['compare'][$key]);
            }
        }

        ksort($data['cm_products_to_category']);
        $cm_categories = array_unique($cm_categories);

        $this->load->model('catalog/category');
        foreach($cm_categories as $cm_category){
            array_push($data['cm_categories'],  $this->model_catalog_category->getCategory($cm_category));
        }


        // ************* /COMPARE ********************


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
        }
    }

    protected function validateAddressForm()
    {

        if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
            $this->as_error['as_city'] = $this->language->get('as_error_city');
        }

        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

        foreach ($custom_fields as $custom_field) {
            if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
                $this->as_error['as_custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('as_error_custom_field'), $custom_field['name']);
            }
        }

        return !$this->as_error;
    }

    public function deleteAddress()
    {
        $this->load->language('account/account');
        $this->load->model('account/address');
        $this->model_account_address->deleteAddress($this->request->get['address_id']);

        // Default Shipping Address
        if (isset($this->session->data['shipping_address']['address_id']) && ($this->request->get['address_id'] == $this->session->data['shipping_address']['address_id'])) {
            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }

        // Default Payment Address
        if (isset($this->session->data['payment_address']['address_id']) && ($this->request->get['address_id'] == $this->session->data['payment_address']['address_id'])) {
            unset($this->session->data['payment_address']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
        }
        $this->session->data['as_success'] = $this->language->get('as_text_delete');


        // Add to activity log
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $this->customer->getId(),
            'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
        );

        $this->model_account_activity->addActivity('address_delete', $activity_data);

        $this->response->redirect($this->url->link('account/account', '', 'SSL') . "#porile");
    }

    protected function validateDeleteAddress()
    {
        if ($this->model_account_address->getTotalAddresses() == 1) {
            $this->error['warning'] = $this->language->get('error_delete');
        }

        if ($this->customer->getAddressId() == $this->request->get['address_id']) {
            $this->error['warning'] = $this->language->get('error_default');
        }

        return !$this->error;
    }

    public function add_wishlist()
    {
        $this->load->language('account/account');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if ($this->customer->isLogged()) {
                // Edit customers cart
                $this->load->model('account/wishlist');

                $this->model_account_wishlist->addWishlist($this->request->post['product_id']);

                $json['success'] = sprintf($this->language->get('wl_text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = sprintf($this->language->get('wl_text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
            } else {
                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }

                $this->session->data['wishlist'][] = $this->request->post['product_id'];

                $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);

                $json['success'] = sprintf($this->language->get('wl_text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = sprintf($this->language->get('wl_text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function profile_validate()
    {
        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['pr_firstname'] = $this->language->get('pr_error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['pr_lastname'] = $this->language->get('pr_error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
            $this->error['pr_email'] = $this->language->get('pr_error_email');
        }

        if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['pr_warning'] = $this->language->get('pr_error_exists');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['pr_telephone'] = $this->language->get('pr_error_telephone');
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

        foreach ($custom_fields as $custom_field) {
            if (($custom_field['location'] == 'account') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
                $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
            }
        }

        return !$this->error;
    }

    protected function password_validate()
    {
        if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
            $this->error['ps_password'] = $this->language->get('ps_error_password');
        }

        if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['ps_confirm'] = $this->language->get('ps_error_confirm');
        }

        return !$this->error;
    }

    public function country()
    {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id' => $country_info['country_id'],
                'name' => $country_info['name'],
                'iso_code_2' => $country_info['iso_code_2'],
                'iso_code_3' => $country_info['iso_code_3'],
                'address_format' => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone' => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status' => $country_info['status']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}