<?php  
class ControllerInformationDeliverypage extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->addScript('catalog/view/javascript/delivery_page.js');

        $this->load->model('catalog/information');
        $data['moscow_cities'] = $this->model_catalog_information->getCitiesMoscow();

        $data['heading_title'] = $this->config->get('config_title');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/deliverypage.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/deliverypage.tpl', $data));
        } else {
                $this->response->setOutput($this->load->view('default/template/information/deliverypage.tpl', $data));
        }
    }
}
?>

