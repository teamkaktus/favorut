<?php  
class ControllerInformationInformationpage extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $data['heading_title'] = $this->config->get('config_title');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
        $data['store'] = $this->config->get('config_name');
        $data['address'] = nl2br($this->config->get('config_address'));
        $data['geocode'] = $this->config->get('config_geocode');
        $data['geocode_hl'] = $this->config->get('config_language');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['fax'] = $this->config->get('config_fax');
        $data['open'] = nl2br($this->config->get('config_open'));
        $data['comment'] = $this->config->get('config_comment');
        $data['config_open'] = $this->config->get('config_open');
        $data['config_services'] = $this->config->get('config_services');
        $data['config_delivery'] = $this->config->get('config_delivery');
        $data['config_garant'] = $this->config->get('config_garant');
        $data['config_payment'] = $this->config->get('config_payment');
//        echo '<pre>';
//        print_r($this->config);
//        echo '</pre>';
//        exit;
//        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/informationpage.tpl')) {
//            $this->template = $this->config->get('config_template') . '/template/information/informationpage.tpl';
//            $data['template'] = $this->config->get('config_template');
//        } else {
//            $this->template = 'default/template/information/informationpage.tpl';
//        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/informationpage.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/informationpage.tpl', $data));
        } else {
                $this->response->setOutput($this->load->view('default/template/information/informationpage.tpl', $data));
        }
        //$this->response->setOutput($this->render());
    }
}
?>

