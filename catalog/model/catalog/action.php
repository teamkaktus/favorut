<?php

class ModelCatalogAction extends Model
{
    public function getAction($action_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "action a LEFT JOIN " . DB_PREFIX . "action_description ad ON (a.action_id = ad.action_id) LEFT JOIN " . DB_PREFIX . "action_to_store a2s ON (a.action_id = a2s.action_id) WHERE a.action_id = '" . (int)$action_id . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND a2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND a.status = '1'");

        return $query->row;
    }

    public function getActions()
    {
        $query = $this->db->query("SELECT a.action_id, title, short_description, image FROM " . DB_PREFIX . "action a LEFT JOIN " . DB_PREFIX . "action_description ad ON (a.action_id = ad.action_id) LEFT JOIN " . DB_PREFIX . "action_to_store a2s ON (a.action_id = a2s.action_id) WHERE ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND a2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND a.status = '1' ORDER BY a.sort_order, LCASE(ad.title) ASC");

        return $query->rows;
    }

    public function getActionProducts($action_id){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_product WHERE action_id = '" .(int)$action_id."'");

        return $query->rows;
    }

    public function getActionProductsHome(){
        $query = $this->db->query("SELECT ap.product_id FROM " . DB_PREFIX . "action a LEFT JOIN " . DB_PREFIX . "action_product ap ON (a.action_id = ap.action_id) GROUP BY ap.action_id LIMIT 0,6");

        return $query->rows;
    }


    public function getActionLayoutId($action_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }
}