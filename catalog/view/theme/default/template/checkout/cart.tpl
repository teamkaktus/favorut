<?php echo $header; ?>
<div class="container">
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> cart_for_print"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?>
                <?php if ($weight) { ?>
                &nbsp;(<?php echo $weight; ?>)
                <?php } ?>
            </h1>

            <?php foreach ($products as $product) { ?>
            <?php if((int)$product['category_id'] != 61){ ?>
            <div class="row cart_product cart_print_product_block" data-product_id="<?=$product['product_id'];?>" style="border-bottom: 1px solid #ebebeb;padding-bottom: 20px;padding-top: 20px">
                <div class="col-lg-7 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                        <div class="col-sm-8 col-lg-7 col-md-8 col-xs-12" style="padding:0px;">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 cart_print_image">
                                <?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                               alt="<?php echo $product['name']; ?>"
                                                                               title="<?php echo $product['name']; ?>"
                                                                               class="img-responsive"/></a>
                                <?php } ?>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 cart_print_select">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 print_name_show" style="padding:0px;">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="padding:0px;">
                                    <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                                    <?php if ($option['type'] == 'select') { ?>
                                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>" <?php if($option['option_id'] == '17'){ echo 'style="display:none;"'; } ?>  >
                                                        <label class="control-label"
                                                               for="input-option<?php echo $option['product_option_id'].$product['cart_id']; ?>"><?php echo $option['name']; ?></label>
                                                        <select name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                                id="input-option<?php echo $option['product_option_id'].$product['cart_id']; ?>" class="changeSelectArrow"
                                                                data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" data-parent="<?php echo $option['product_option_id']; ?>">
                                                            <!--<option value=""><?php echo $text_select; ?></option>-->
                                                            <?php foreach ($option['product_option_value'] as $option_value) {
                                                      if($option_value['selected'] == 1){ ?>
                                                            <option selected
                                                                    value="<?php echo $option_value['product_option_value_id']; ?>" data-hint="<?=$option_value['hint'];?>"><?php echo $option_value['name']; ?>
                                                                <?php }else{ ?>
                                                            <option value="<?php echo $option_value['product_option_value_id']; ?>" data-hint="<?=$option_value['hint'];?>"><?php echo $option_value['name']; ?>
                                                                <?php } if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </option>
                                                            <?php } ?>
                                                        </select>
                                                        <div class="cart-show-hint"></div>
                                                    </div>
                                                    <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="padding:0px;">
                                    <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <?php if ($option['type'] == 'checkbox') { ?>
                                            <div class="cart-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <?php if($option_value['selected'] == 1){ ?>
                                                            <?php echo $option_value['name']; ?>
                                                            <div class="checkbox" style="height:0px;">
                                                                <label style="padding-left: 0px">
                                                                        <input type="checkbox" checked
                                                                               name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>][]"
                                                                               value="<?php echo $option_value['product_option_value_id']; ?>"
                                                                               data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" style="display:none;" />
                                                                </label>
                                                            </div>
                                                            <?php }else{ ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-4 col-md-4 col-lg-5 col-xs-12">
                                    <?php if (!$product['stock']) { ?>
                                    <span class="text-danger">***</span>
                                    <?php } ?>
                                    <?php if ($product['option']) { ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                    <div class="cart-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="radio">
                                                <label>
                                                    <?php if($option_value['selected'] == 1){ ?>
                                                    <input type="radio" checked
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php }else{ ?>
                                                    <input type="radio"
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php } ?>
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if ($option['type'] == 'image') { ?>
                                    <div class="cart_print_color cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?> col-md-12 col-sm-12 col-lg-6 col-xs-12" style="padding:1px;">
                                        <label class="control-label controlLabelImageOption" style="color:#2e76b4;border-bottom: 1px dashed #2e76b4"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id'].$product['cart_id']; ?>">
                                            <div  class="allColor">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <?php if($option_value['selected'] == 1){ ?>
                                                            <div  style='float:left;padding-left:5px;'>
                                                                <label>
                                                                    <input type="radio" checked
                                                                        name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                                        value="<?php echo $option_value['product_option_value_id']; ?>"
                                                                        data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" class="inputColor" style='display:none;' />
                                                                        <img src="<?php echo $option_value['image']; ?>"
                                                                             alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                             class="img-thumbnail" style='height:25px;'  data-toggle="tooltip" data-placement="bottom" title="<?php echo $option_value['name'] ?>"  /> 
                                                                </label>
                                                            </div>
                                                        <?php }else{ ?>
                                                            <div  style='float:left;padding-left:5px;'>
                                                                <label>
                                                                    <input type="radio"
                                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" class="inputColor" style='display:none;' />
                                                                            <img src="<?php echo $option_value['image']; ?>"
                                                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                                 class="img-responsive" style='height:25px;'  data-toggle="tooltip" data-placement="bottom" title="<?php echo $option_value['name'] ?>" /> 
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                <?php } ?>
                                            </div>
                                            
                                            <div class="oneColor">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['selected'] == 1){ ?>
                                                        <div  style='float:left;padding-left:5px;'>
                                                            <label>
                                                                <img src="<?php echo $option_value['image']; ?>"
                                                                         alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                         class="img-thumbnail" style='height:25px;'  data-toggle="tooltip" data-placement="bottom" title="<?php echo $option_value['name'] ?>" /> 
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text"
                                               name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                               value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
                                               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"
                                               data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea
                                                name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                rows="5" placeholder="<?php echo $option['name']; ?>"
                                                id="input-option<?php echo $option['product_option_id']; ?>"
                                                class="form-control"
                                                data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"><?php echo $option['value']; ?></textarea>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>"
                                                data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block">
                                            <i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden"
                                               name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                               value="" id="input-option<?php echo $option['product_option_id']; ?>"
                                               data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>


                                    <?php } ?>
                                    <?php if ($product['reward']) { ?>
                                    <br/>
                                    <small><?php echo $product['reward']; ?></small>
                                    <?php } ?>
                                    <?php if ($product['recurring']) { ?>
                                    <br/>
                                    <span class="label label-info"><?php echo $text_recurring_item; ?></span>
                                    <small><?php echo $product['recurring']; ?></small>
                                    <?php } ?>
                    </div>
                </div>
                </div>
                <div class="col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                       <span class="to_print">Количество - </span>
                       <div class="input-group parent_block_cart cart_<?=$product['cart_id'];?>">
                            <span class="input-group-btn no_print"">
                                <button class="dec-c-quantity btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px" data-cart="<?php echo $product['cart_id']; ?>">-</button>
                            </span>
                            <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]"
                                   data-cart_id="<?php echo $product['cart_id']; ?>"
                                   value="<?php echo $product['quantity']; ?>" size="1"
                                   class="form-control input-quantity" style="border:0px solid transparent;text-align:center;" />
                            <span class="input-group-btn no_print"">
                                <button class="inc-c-quantity btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px" data-cart="<?php echo $product['cart_id']; ?>">+</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <span>
                            <span class="to_print">Цена - </span><b style="font-size:20px;color:black;" class="product_<?php echo $product['cart_id']; ?>_price"><?php echo $product['total']; ?></b>
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 no_print">
                        <div class="col-md-12 col-xs-6" style="padding:0px;padding-bottom:5px;">
                            <a data-toggle="tooltip" data-original-title="В список желаний" style="font-size:11px;cursor: pointer" onclick="wishlist.add(<?=$product['product_id'];?>);"><div class="header_heart_icon" style="margin-top: 5px;height:15px;width:15px;float:left;"></div><h4 style="margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;">В желания</h4></a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-6" style="padding:0px;">
                            <a  style="cursor:pointer;"  onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="glyphicon glyphicon-remove" style="color:red;color:#98070a;font-size:15px;"><span style="padding-left: 6px;color:#387ab0;">Удалить</span></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
            
            
            
            <div class='row'>
            <?php foreach ($products as $product) { ?>
                <?php if($product['category_id'] == '61'){ ?>
                    <div class="row cart_product cart_print_product_block" data-product_id="<?=$product['product_id'];?>" style="border-bottom: 1px solid #ebebeb;padding-bottom: 20px;padding-top: 20px">
                <div class="col-lg-7 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                        <div class="col-sm-8 col-lg-7 col-md-8 col-xs-12" style="padding:0px;">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 cart_print_image">
                                <?php if ($product['thumb']) { ?>
                                <img src="<?php echo $product['thumb']; ?>"
                                    alt="<?php echo $product['name']; ?>"
                                    title="<?php echo $product['name']; ?>"
                                    class="img-responsive"/>
                                <?php }else{ ?>
                                    <img src="../../../image/catalog/services_image.png"
                                        alt="<?php echo $product['name']; ?>"
                                        title="<?php echo $product['name']; ?>"
                                        class="img-responsive"/>
                                <?php } ?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 cart_print_select">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 print_name_show" style="padding:0px;">
                                    <?php echo $product['name']; ?>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="padding:0px;">
                                    <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <?php if ($option['type'] == 'select') { ?>
                                            <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <select name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                        id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow"
                                                        data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" data-parent="<?php echo $option['product_option_id']; ?>">
                                                    <!--<option value=""><?php echo $text_select; ?></option>-->
                                                    <?php foreach ($option['product_option_value'] as $option_value) {
                                              if($option_value['selected'] == 1){ ?>
                                                    <option selected
                                                            value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php }else{ ?>
                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php } if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-4 col-md-4 col-lg-5 col-xs-12">
                                    <?php if (!$product['stock']) { ?>
                                    <span class="text-danger">***</span>
                                    <?php } ?>
                                    <?php if ($product['option']) { ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                    <div class="cart-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="radio">
                                                <label>
                                                    <?php if($option_value['selected'] == 1){ ?>
                                                    <input type="radio" checked
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php }else{ ?>
                                                    <input type="radio"
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php } ?>
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                                    )
                                                    <?php } ?>
                                                </label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                    <div class="cart-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="checkbox">
                                                <label>
                                                    <?php if($option_value['selected'] == 1){ ?>
                                                    <input type="checkbox" checked
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>][]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php }else{ ?>
                                                    <!--<input type="checkbox"
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>][]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                                    <?php } ?>
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?> 
                                                    ) -->
                                                    <?php } ?>
                                                </label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                    <div class="cart_print_color cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?> col-md-12 col-sm-12 col-lg-6 col-xs-12" style="padding:1px;">
                                        <label class="control-label" style="color:#2e76b4;border-bottom: 1px dashed #2e76b4"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div  style='float:left;padding-left:5px;'>
                                                <label>
                                                    <?php if($option_value['selected'] == 1){ ?>
                                                    <input type="radio" checked
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" class="inputColor" style='display:none;' />
                                                            <img src="<?php echo $option_value['image']; ?>"
                                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                 class="img-thumbnail" style='height:25px;'/> 
                                                    <?php }else{ ?>
                                                    <input type="radio"
                                                           name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>" class="inputColor" style='display:none;' />
                                                            <img src="<?php echo $option_value['image']; ?>"
                                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                 class="img-responsive" style='height:25px;'/> 
                                                    <?php } ?>
                                                    <!--<?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                                    )
                                                    <?php } ?> -->
                                                </label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text"
                                               name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                               value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
                                               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"
                                               data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea
                                                name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                rows="5" placeholder="<?php echo $option['name']; ?>"
                                                id="input-option<?php echo $option['product_option_id']; ?>"
                                                class="form-control"
                                                data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"><?php echo $option['value']; ?></textarea>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>"
                                                data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block">
                                            <i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden"
                                               name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                               value="" id="input-option<?php echo $option['product_option_id']; ?>"
                                               data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                    <div class="cart-form-group<?php echo ($option['required'] ? ' required' : ''); ?> cart_<?php echo $product['cart_id']; ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text"
                                                   name="option[<?=$product['cart_id'];?>][<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control" data-cart_id="<?php echo $product['cart_id']; ?>" data-product_id="<?=$product['product_id'];?>"/>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>


                                    <?php } ?>
                                    <?php if ($product['reward']) { ?>
                                    <br/>
                                    <small><?php echo $product['reward']; ?></small>
                                    <?php } ?>
                                    <?php if ($product['recurring']) { ?>
                                    <br/>
                                    <span class="label label-info"><?php echo $text_recurring_item; ?></span>
                                    <small><?php echo $product['recurring']; ?></small>
                                    <?php } ?>
                    </div>
                </div>
                </div>
                <div class="col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                       <span class="to_print">Количество - </span>
                       <div class="input-group parent_block_cart cart_<?=$product['cart_id'];?>">
                            <span class="input-group-btn no_print"">
                                <button class="dec-c-quantity btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px" data-cart="<?php echo $product['cart_id']; ?>">-</button>
                            </span>
                            <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]"
                                   data-cart_id="<?php echo $product['cart_id']; ?>"
                                   value="<?php echo $product['quantity']; ?>" size="1"
                                   class="form-control input-quantity" style="border:0px solid transparent;text-align:center;" />
                            <span class="input-group-btn no_print"">
                                <button class="inc-c-quantity btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px" data-cart="<?php echo $product['cart_id']; ?>">+</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <span>
                            <span class="to_print">Цена - </span><b style="font-size:20px;color:black;" class="product_<?php echo $product['cart_id']; ?>_price"><?php echo $product['total']; ?></b>
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 no_print">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                            <a  style="cursor:pointer;"  onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="glyphicon glyphicon-remove" style="color:red;color:#98070a;font-size:15px;"><span style="padding-left: 6px;color:#387ab0;">Удалить</span></i></a>
                        </div>
                    </div>
                </div>
            </div>
                <?php } ?>
            <?php } ?>
            </div>
            
            
            
            <!--<?php foreach ($vouchers as $vouchers) { ?>
            <tr>
              <td></td>
              <td class="text-left"><?php echo $vouchers['description']; ?></td>
              <td class="text-left"></td>
              <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                  <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                  <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
              <td class="text-right"><?php echo $vouchers['amount']; ?></td>
              <td class="text-right"><?php echo $vouchers['amount']; ?></td>
            </tr>
            <?php } ?>-->

            <div class="row no_print" style="padding-top: 20px">
                <div class="col-sm-2 col-md-3 col-lg-2 hidden-xs">
                    <div class="pull-left">
                        <a href="<?php echo $continue; ?>"
                           class="btn" style="border-radius:0px;background-color: #f4f4f4 !important;"><?php echo $button_shopping; ?></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs" style="padding:0px;">
                    <div class="col-lg-4 col-md-12 col-sm-12 hiden-xs" style="padding-top: 5px;padding-bottom: 15px">
                        <div class="col-md-5 col-lg-12 col-sm-5" style='cursor:pointer;' onclick="window.print()">Распечатать</div>
                        <div style='clear:both;'></div>
                        <div class="col-md-7 col-lg-12 col-sm-7" style='cursor:pointer;' id="all_add_wishlist">
                            В желания ввесь список
                        </div>
                        <div style='clear:both;'></div>
                        <div id="wishlist-success-text"></div>
                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 cart_icon_background" style="padding:0px;">
                        <div class="col-sm-3">
                           <div class="cart_money_icon" style="height:55px;width: 55px"></div>
                        </div>
                        <div class="col-sm-9" style="padding:0px;">
                               Если товар не подошел, вернём деньги <br>в течение 7 дней с момента его получения
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-3" style="text-align:center;">
                    <div class="row">
                        <?php foreach ($totals as $total) {
                            if($total['title'] == 'Итого'){
                        ?>
                        <!--<div class="col-sm-4"><?php echo $total['title']; ?>:</div> -->
                        <div class="col-sm-12"><h1 style="margin-top: 0px"><strong id="cart_total_price"><?php echo $total['text']; ?></strong></h1></div>

                        <?php }
                        } ?>
                    </div>
                    <div class="row">
                        <a href="<?php echo $checkout; ?>" class="btn" style="border-radius:0px;background-color: #f9a31b;border:0px solid transparent;color:black;"><?php echo $button_checkout; ?></a>
                    </div>
                </div>
                <div class="hidden-lg hidden-md hidden-sm col-xs-12 cart_icon_background" style="background-repeat:repeat-y !important;padding:0px;padding-top:10px;margin-top:20px;">
                    <div class="col-xs-3">
                       <div class="cart_money_icon" style="height:55px;width: 55px"></div>
                    </div>
                    <div class="col-xs-9" style="padding:0px;">
                           Если товар не подошел, вернём деньги <br>в течение 7 дней с момента его получения
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
