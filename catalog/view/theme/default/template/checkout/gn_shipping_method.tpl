<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p><?php echo $text_shipping_method; ?></p>
<?php foreach ($shipping_methods as $shipping_method) { ?>
<p><strong><?php echo $shipping_method['title']; ?></strong></p>
<?php if (!$shipping_method['error']) { ?>
<?php foreach ($shipping_method['quote'] as $quote) { ?>
<div class="radio">
  <label for="<?=$quote['code'];?>">
    <?php if ($quote['code'] == $code || !$code) { ?>
    <?php $code = $quote['code']; ?>
    <input type="radio" name="shipping_method" id="<?=$quote['code'];?>" value="<?php echo $quote['code']; ?>" title="<?php echo $quote['title']; ?>" checked="checked" />
    <?php } else { ?>
    <input type="radio" name="shipping_method" id="<?=$quote['code'];?>" value="<?php echo $quote['code']; ?>" title="<?php echo $quote['title']; ?>" />
    <?php } ?>
    <span class="<?=str_replace(".", "_", $quote['code']);?>_text"><?php echo $quote['title']; ?></span> - <span class="<?=str_replace(".", "_", $quote['code']);?>_price"><?php echo $quote['text']; ?></span>
    <p class="<?=str_replace('.', '_', $quote['code']);?>_info" style="padding: 5px;"></p>
  </label>

</div>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
<?php } ?>
