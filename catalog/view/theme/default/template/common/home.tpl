<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-8">
                <?php echo $content_top['modules']['0']; ?>
            </div>
            <div class="col-sm-12 hidden-xs col-md-12 col-lg-4">
                <h3 style='margin:0px;margin-bottom:5px;'><a href="<?php echo $all_special; ?>" style="text-decoration: underline">Акции</a></h3>
                <div class="row">
                    <?php foreach ($products as $key => $product) { ?>
                    <?php
                        $class = '';
                        $class2 = '';
                        $color = '';
                        switch($key) {
                            case 0:
                                $class = ' action_fon_home_page ';
                                $color = '#464c8d';
                                break;
                            case 1:
                                $class = ' action_fon_home_page_2 ';
                                $color = '#8ebde3';
                                break;
                            case 2:
                                $class = ' action_fon_home_page_3 ';
                                $color = '#ed703f';
                                break;
                            case 3:
                                $class = ' action_fon_home_page_3 ';
                                $class2 = ' hidden-lg ';
                                $color = '#464c8d';
                                break;
                            case 4:
                                $class = ' action_fon_home_page_2 ';
                                $class2 = ' hidden-lg ';
                                $color = '#8ebde3';
                                break;
                            case 5:
                                $class = 'action_fon_home_page ';
                                $class2 = ' hidden-lg ';
                                $color = '#464c8d';
                                break;
                            }
                      ?>
                    <div class="product-layout col-lg-4 col-md-2 col-sm-2 col-xs-12 <?php echo $class2; ?>" style='padding:5px;'>
                        <div class="product-thumb <?php echo $class; ?>" style="margin-bottom: 0px;min-height:200px;">
                            <div class="image" style='padding:10px;padding-bottom: 0px'>
                                <a href="<?php echo $product['href']; ?>" ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" style="border-radius: 100%;width:80px;border:6px solid <?php echo $color; ?>;" /></a>
                            </div>
                            <div>
                                <div class="caption" style="padding:10px;min-height: 0px">
                                    <h5 style='margin:0px;'><a href="<?php echo $product['href']; ?>" style='color:white;'><b><?php echo $product['name']; ?></b></a></h5>
                                    <div class='col-sm-12 col-xs-12 col-md-12' style="padding:0px;">
                                        <?php if ($product['price']) { ?>
                                        <p class="price" style='margin-bottom: 5px;margin-top: 10px'>
                                            <?php if ($product['special']) { ?>
                                            <span class="price-old" style='color:white;margin-left: 0px'><?php echo $product['price']; ?></span>
                                            <span class="price-new" style="font-size:15px;color:white;font-weight: bold;"><?php echo $product['special']; ?></span>
                                            <?php }else{ ?>
                                                <span class="price-new" style='color:white;margin-left: 0px'><?php echo $product['price']; ?></span>
                                            <?php }?>
                                        </p>
                                        <?php } ?>
                                        <button type="button" style='width:100%;background-color: #292f7b;margin-bottom:8px;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 hidden-xs" style="padding-top: 20px;padding-bottom: 20px">
                <?php foreach ($manufactures as $key => $manufacturer) { ?>
                    <?php if($key > 5){ break; }?>
                    
                    <?php if(($key >= 0) and ($key <= 3)){ $classBB = ' col-sm-3 '; } ?>
                    <?php if(($key == 4) || ($key == 5)){ $classB = ' hidden-sm '; }?>
                    <div class="<?= $classBB; ?> <?= $classB; ?> col-lg-2 col-md-2" style="text-align:center;">
                        <a href="<?php echo $manufactures_url.'/info&manufacturer_id='.$manufacturer['manufacturer_id']; ?>"><img title="<?php echo $manufacturer['name']; ?>" src="../../../image/<?= $manufacturer['image']; ?>" style="height:60px;"></a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-sm-2" style="padding-top:30px;">
                 <a href='<?= $manufactures_url; ?>' style='text-decoration: underline'>Все бренды</a>
            </div>
        </div>
        <div class="row hidden-sm hidden-md hidden-lg" style='padding-bottom: 15px'>
            <div class="col-xs-6">
                <button class='btn' style='width: 100%;background-color: #ea5b23 !important;text-decoration: underline'>
                    <h3 style='margin-top: 5px;'>
                        <a href='<?php echo $all_special; ?>' style='color:white;'>Акции</a>
                    </h3>
                </button> 
            </div>
            <div class="col-xs-6">
                <button class='btn' style='width: 100%;background-color: #292f7b !important;'>
                    <h3 style='margin-top: 5px;'>
                        <a href='<?= $manufactures_url; ?>' style='color:white;text-decoration: underline'>Бренды</a>
                    </h3>
                </button>
            </div>
        </div>
        
        <div class="col-sm-12">
            <ul id="popular_new_tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="#popular_t" data-toggle="tab"><b>Популярные модели</b></a></li>
                <li><a href="#new_t" data-toggle="tab"><b>Новые поступления</b></a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="popular_t">
                    <?php echo $content_top['modules']['3']; ?>
                </div>
                <div class="tab-pane" id="new_t">
                    <?php echo $content_top['modules']['4']; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12" style="padding-left:0px;padding-right: 0px">
            <?php echo $content_top['modules']['7']; ?>
        </div>
        <div class="row" style="border-bottom:1px solid #e3e3e3;margin-bottom: 20px;">
            <div class="col-md-7 col-lg-8 col-sm-12">
            </div>
            <div class="col-md-5 col-lg-12 hidden-xs hidden-sm">
                <?php echo $content_top['modules']['6']; ?>
            </div>
        </div>
        <div class="row hidden-md hidden-lg" style="border-bottom:1px solid #e3e3e3;margin-bottom: 20px;">
            <div class="hidden-md hidden-lg col-sm-6 col-xs-12">
                <?php echo $content_top['modules']['6']; ?>
            </div>
            <div class="hidden-md hidden-lg col-sm-6 col-xs-12">
                <div class="call_back_banner" style="width:100%;height:300px;padding:20px;">
                    <h3 style="margin-top: 5px"><b>Трудно вибрать?</b></h3>
                    <h2 style="color:#98070a;margin-top:5px;"><b>ПОМОЖЕМ</b></h2>
                    <h5>Расскажем плюсы и минусы,<br> найдём подходяшие варианты</h5>
                    <button style="padding:5px 15px 5px 15px;border:0px solid transparent;background-color:#98070a;margin-top: 10px;color:white;">Заказать звонок</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <?php echo $content_top['modules']['5']; ?>
            </div>
            <div class="col-md-5 col-lg-4 hidden-sm hidden-xs">
                <div class="call_back_banner" style="width:100%;height:300px;padding:20px;">
                    <h3 style="margin-top: 5px"><b>Трудно вибрать?</b></h3>
                    <h2 style="color:#98070a;margin-top:5px;"><b>ПОМОЖЕМ</b></h2>
                    <h5>Расскажем плюсы и минусы, найдём подходяшие варианты</h5>
                    <button style="padding:5px 15px 5px 15px;border:0px solid transparent;background-color:#98070a;margin-top: 10px;color:white;">Заказать звонок</button>
                </div>
            </div>
        </div>
        
        <?php //echo $content_top; ?><?php //echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<?php echo $footer; ?>