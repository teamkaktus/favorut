<div id="search" class="input-group hidden-xs">
  <input type="text" class="searchInput" name="search" style="display:none;position:absolute;left:25px;height:30px;width: 300px;border:1px solid #e2e2e2;" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
  <button type="button" class="searchButton" style="height:30px;margin-left:-5px;width: 30px;background-color: #f9a51e;border:1px solid #e2e2e2;"><i class="fa fa-search" style="color:white;"></i></button>
</div>
<a id="searchS" class="input-group hidden-lg hidden-md hidden-sm" style="cursor:pointer;padding-top:15px;">
    <i class="fa fa-search" style="color:white;"></i>
</a>
<div id="searchMobile" class="input-group" style="display:none;margin-bottom: 5px;margin-top:5px;width:100%;">
  <input type="text" name="search" style="height:37px;width: 90%;border:1px solid #e2e2e2;" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
  <button type="button" style="height:37px;margin-left:-5px;width: 10%;background-color: white;border:1px solid #e2e2e2;"><i class="fa fa-search" style="color:#f9a51e;"></i></button>
</div>