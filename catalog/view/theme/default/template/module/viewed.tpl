<div class="row product_nedavno_pereg" style="padding-left:20px; padding-right:20px;">
<div class="col-sm-12">
    <h3 style="margin-top:10px;">Вы недавно смотрели</h3>
</div>
<div class="row">
  <?php foreach ($products as $key => $product) { ?>
    <?php 
    $class = '';
        if($key > 3){
            $class = "hidden-md hidden-sm hidden-xs";
        }
    ?>
<div class="product-layout  col-lg-2 col-md-3 col-sm-3 col-xs-12 <?php echo $class; ?>" style="padding-left: 5px;padding-right: 5px">
    <div class="product-thumb" style='background-color: white'>
        <div class="image" style='padding:10px;'>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
            <?php if ($product['special']) { ?>
                <div class='action_product_icon' style='height:50px;width:50px;position:absolute;z-index:99;right:-8px;top:-10px;'></div>
           <?php } ?>
        </div>
        <div class="caption col-sm-12" style="min-height:150px;padding-bottom: 10px;padding-left: 7px;padding-right: 7px">
            <div style="min-height: 100px;">
                <div class="col-sm-12" style="padding:0px;">
                    <h4 style="margin-bottom: 2px"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                    <div class='col-sm-12' style='padding:0px;'>
                        <?php foreach($product['attribute_groups'] as $product_attribute){
                                if($product_attribute['attribute_group_id'] == '7'){
                                    foreach($product_attribute['attribute'] as $attrib){
                                        echo '<span style="color:#727272;">'.$attrib['text'].'</span><br />';
                                    };
                                }
                        }; ?>
                    </div>

                    <div class='col-sm-12' style='padding:0px;'>
                        <?php if (!$product['special']) { ?>
                        <?php } else { ?>
                            <span class="price-old" style='color:#98070a;'><?php echo $product['price']; ?></span>
                        <?php } ?>

                    </div>
                    <div class='col-sm-8 col-xs-8 col-md-7' style="padding:0px;">
                        <?php if ($product['price']) { ?>
                        <p class="price">
                          <?php if (!$product['special']) { ?>
                          <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price']; ?></span><span style="font-size:9px;" >/полотно</span>
                          <?php } else { ?>
                          <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special']; ?></span><span >/полотно</span> <!--<span class="price-old"><?php echo $product['price']; ?> </span> -->
                          <?php } ?>
                        </p>
                        <?php } ?>
                    </div>
                    <div class='col-sm-4 col-xs-4 col-md-5' style='padding:0px;'>
                        <?php if (!$product['special']) { ?>
                        <?php if($product['stock'] > 0){ ?>
                            <div style='padding:2px 1px 2px 2px;font-size:9px;background-color: #f9a51e;color:black;text-align:center;'><b>в наличии</b></div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 hidden-lg hidden-lg hidden-sm hidden-md" >
                <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                    <div class="col-sm-5" style="padding:2px;">
                        <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                    </div>
                    <div class="col-sm-7" style="padding:2px;">
                        <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                    <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                        <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                    <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                        <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                </div>
            </div>
            
            <div class="product_bottom_info hidden-xs" style='z-index: 100;bottom:-48px;'>
                <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                    <div class="col-sm-5" style="padding:2px;">
                        <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                    </div>
                    <div class="col-sm-7" style="padding:2px;">
                        <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                    <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                        <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                    <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                        <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                </div>
            </div>
    </div>
</div>
  <?php } ?>
</div>
</div>
