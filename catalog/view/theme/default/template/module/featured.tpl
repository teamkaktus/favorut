<h3><?php echo $heading_title; ?></h3>
<div class="row">
  <?php foreach ($products as $product) { ?>
  <div class="product-layout  col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="product-thumb">
            <div class="image" style='padding:10px;'>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                <?php if (!$product['special']) { ?>
                    <?php if($product['stock'] > 0){ ?>
                        <div style='padding:4px 27px 4px 27px;background-color: #f9a51e;position:absolute;color:black;top:20px;'><b>в наличии</b></div>
                    <?php } ?>
                <?php }else{ ?>
                    <div class='action_product_icon' style='height:50px;width:50px;position:absolute;right:-10px;top:-10px;'></div>
               <?php } ?>
            </div>
            <div>
              <div class="caption" style="padding-bottom: 10px">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <div class='col-sm-12' style='padding:0px;'>
                    <?php foreach($product['attribute_groups'] as $product_attribute){
                            if($product_attribute['attribute_group_id'] == '7'){
                                foreach($product_attribute['attribute'] as $attrib){
                                    echo '<span style="color:#727272;">'.$attrib['text'].'</span><br />';
                                };
                            }
                    }; ?>
                </div>
                
                <div class='col-sm-12' style='padding:0px;height:15px;'>
                    <?php if (!$product['special']) { ?>
                    <?php } else { ?>
                        <span class="price-old" style='color:#98070a;'><?php echo $product['price']; ?></span>
                    <?php } ?>
                </div>
                <div class='col-sm-8 col-xs-8 col-md-7' style="padding:0px;">
                    <?php if ($product['price']) { ?>
                    <p class="price">
                      <?php if (!$product['special']) { ?>
                      <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price']; ?></span><span >/полотно</span>
                      <?php } else { ?>
                      <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special']; ?></span><span >/полотно</span> <!--<span class="price-old"><?php echo $product['price']; ?> </span> -->
                      <?php } ?>
                    </p>
                    <?php } ?>  
                </div>
                <div class='col-sm-4 col-xs-4' style='padding:0px;'>
                    <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button> -->
                    <a data-toggle="tooltip" style="font-size:14px;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">Сравнить</a>
                </div>
              
                <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
              </div>
            </div>
          </div>
        </div>
  <?php } ?>
</div>
