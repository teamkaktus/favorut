<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
<div id="container" style="margin-left: 15px;margin-right: 15px">
    <h1>Оплата</h1>
    <div class="row">
        <div class='col-sm-6'>
            <div class="col-sm-12 col-xs-12">
                <div class="delivery_icon_nal" style="width:35px;height:35px;float:left;"></div>
                <h2 style="float:left;margin-top:0px;margin-left: 5px">Наличными</h2>
            </div>
            <div class="col-sm-12 col-xs-12">
                <ul>
                    <li>
                        <p><b>Оплата при самовывозе</b></p>
                        <p>Через платежную систему RBS. После оформления и оплаты заказа с Вами свяжется наш менеджер, чтобы подтвердить заказ и уточнить срок доставки.</p>
                    </li>
                    <li>
                        <p>
                            <b>
                                Оплата курьеру
                            </b>
                        </p>
                        <p>
                            После оформления Заказа наш менеджер свяжется с Вами, чтобы подтвердить заказ и уточнить срок доставки. После этого к Вам приедет наш курьер с мобильным банковским терминалом.
                        </p>
                    </li>

                </ul>
            </div>
        </div>
        <div class='col-sm-6'>
            <div class="col-sm-12 col-xs-12">
                <div class="delivery_icon_beznal" style="width:35px;height:35px;float:left;"></div>
                <h2 style="float:left;margin-top:0px;margin-left: 5px">Безналичный расчет</h2>
            </div>
            <div class="col-sm-12 col-xs-12">
                <ul>
                    <li>
                        <p><b>Перевод/платеж по квитанции в любом банке</b></p>
                        <p>заполненный бланк для оплаты будет выслан вам после подтверждения заказа вместе со счетом.</p>
                    </li>
                    <li>
                        <p>
                            <b>
                                Перевод со своего счета на счет получателя
                            </b>
                        </p>
                        <p>
                            банковские реквизиты для оплаты будут высланы вам после подтверждения заказа вместе со счетом
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-6'>
            <div class="col-sm-12 col-xs-12">
                <div class="delivery_icon_cart" style="width:35px;height:35px;float:left;"></div>
                <h2 style="float:left;margin-top:0px;margin-left: 5px">Картами</h2>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class='col-sm-12' style='padding-left: 40px'>
                    <p>Заказ должен быть оплачен полностью, перед доставкой товара</p>
                    <div class='icon_visa_pay' style='height:30px;width: 50px;float:left;'></div>
                    <div class='icon_masercard_pay' style='height:30px;width: 50px;float:left;'></div>
                </div>
                <div class='col-sm-12'>
                    <ul>
                        <li>
                            <p><b>Оплата на сайте</b></p>
                            <p>Через платежную систему RBS. После оформления и оплаты заказа с Вами свяжется наш менеджер, чтобы подтвердить заказ и уточнить срок доставки.</p>
                        </li>
                        <li>
                            <p>
                                <b>
                                    Оплата при доставке
                                </b>
                            </p>
                            <p>
                                После оформления Заказа наш менеджер свяжется с Вами, чтобы подтвердить заказ и уточнить срок доставки. После этого к Вам приедет наш курьер с мобильным банковским терминалом.
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class='col-sm-6'>
            <div class="col-sm-12 col-xs-12">
                <div class="delivery_icon_elemoney" style="width:35px;height:35px;float:left;"></div>
                <h2 style="float:left;margin-top:0px;margin-left: 5px">Электронными деньгами</h2>
            </div>
            <div class='col-sm-12' style='padding-left: 40px'>
                <p>Заказ должен быть оплачен полностью, перед доставкой товара.</p>
                <div class='icon_yandex_pay' style='height:30px;width: 50px;float:left;'></div>
            </div>
            <div class="col-sm-12 col-xs-12" style='padding-left: 40px'>
                <p>Через платёжную систему Яндекс.Деньги. З
аполнив форму оплаты в нашем магазине, вы будете перенаправлены на сайт Яндекс.Денег, где сможете завершить платеж.</p>
            </div>
        </div>
    </div>
</div>
      <!--
    <div id="container" style="margin-left: 15px;margin-right: 15px">
        <h1>Оплата</h1>
        <div class="row">
            <div class="col-sm-9">
                <p>В любую точку Российской Федерации!<br>
                    Услуги по доставке заказов, сделанных через наш интернет-магазин, оказываются специализированной транспортной компанией. 
                    Перед погрузкой наши сотрудники осуществляют тщательный осмотр товара, уделяя особое внимание внешнему виду и комплектации. 
                    Доставка заказов осуществляется «до двери»
                </p>
            </div>
            <div class="col-sm-6">
                <h2>По Москве и области</h2>
                    <h4>
                    Местонахождение:
                    <select class="btn">
                        <option>Щелково</option>
                        <option>Москва</option>
                    </select>
                    </h4>
                <p>
                    Как забрать или любая другая информация В любую точку Российской Федерации!
                    Услуги по доставке заказов, сделанных через наш интернет-магазин, оказываются 
                    специализированной транспортной компанией. 
                </p>
                <p>
                    Перед погрузкой наши сотрудники 
                    осуществляют тщательный осмотр товара, уделяя особое внимание внешнему виду 
                    и комплектации. Доставка заказов осуществляется «до двери».
                </p>
            </div>
            <div class="col-sm-6">
                <h2>По России</h2>
                <b>Доставка в любой регион России</b>
                <ul>
                    <li>
                        Бесплатно до транспортной компании. 
                    </li>
                    <li>
                        Оплата: 100%. 
                    </li>
                    <li>
                        Доставкатоваров под заказ:  25-30 раб.дн. Оплата: 50%
                    </li>
                </ul>
            </div>
            <?php //echo $content_top; ?>
            <?php //echo $content_bottom; ?>
         </div>
     </div> -->
<?php echo $footer; ?>