<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>

    <div id="container" style="margin-left: 15px;margin-right: 15px">
        <h1>Доставка</h1>
        <div class="row">
            <div class="col-sm-9">
                <p>В любую точку Российской Федерации!<br>
                    Услуги по доставке заказов, сделанных через наш интернет-магазин, оказываются специализированной транспортной компанией. 
                    Перед погрузкой наши сотрудники осуществляют тщательный осмотр товара, уделяя особое внимание внешнему виду и комплектации. 
                    Доставка заказов осуществляется «до двери»
                </p>
            </div>
            <div class="col-sm-6 moskva">
                <h2 style='margin-top: 50px;margin-bottom: 50px'>По Москве и области</h2>
                    <h4>
                    Местонахождение:
                    <select class="changeSelectArrow" style='width:auto;' id="select_moscow">
                        <?php
                        foreach($moscow_cities as $city){ ?>
                            <option value="<?=$city;?>"><?=$city;?></option>
                        <?php } ?>
                    </select>
                    <span id="moscow_price"></span>
                    </h4>
                <p>
                    Как забрать или любая другая информация В любую точку Российской Федерации!
                    Услуги по доставке заказов, сделанных через наш интернет-магазин, оказываются 
                    специализированной транспортной компанией. 
                </p>
                <p>
                    Перед погрузкой наши сотрудники 
                    осуществляют тщательный осмотр товара, уделяя особое внимание внешнему виду 
                    и комплектации. Доставка заказов осуществляется «до двери».
                </p>
            </div>
            <div class="col-sm-6 information_icon_russia">
                <h2 style='margin-top: 50px;margin-bottom: 50px'>По России</h2>
                <b>Доставка в любой регион России</b>
                <ul>
                    <li>
                        Бесплатно до транспортной компании. 
                    </li>
                    <li>
                        Оплата: 100%. 
                    </li>
                    <li>
                        Доставкатоваров под заказ:  25-30 раб.дн. Оплата: 50%
                    </li>
                </ul>
                <b>Стоимость доставки</b>
                <div class="row">
                    <div class="col-sm-2">В город:</div>
                    <div class="col-sm-7">
                        <form action="">
                            <div class="form_city">
                                <input type="text" class="autocomplete_city">
                                <div class="dropdown_city">
                                    <ul class="list_cities">

                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-3">
                        <span id="russian_price"></span>
                    </div>
                </div>
            </div>
            <div id="hide_map"></div>
            <?php //echo $content_top; ?>
            <?php //echo $content_bottom; ?>
         </div>
     </div>
<?php echo $footer; ?>