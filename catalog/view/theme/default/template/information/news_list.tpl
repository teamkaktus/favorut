<?php echo $header; ?>
<div class="container">
  <!--<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>" style="min-height: 400px">
      <h1>Новости</h1>
            <div class="row">
                <?php foreach ($all_news as $news) { ?>
                <div class="product-layout col-lg-2">
                    <div  style="border:1px solid #f1f1f1;padding:10px;">
                        <?php if($news['image'] != null){ ?>
                            <div class="col-sm-12">
                                <img src="<?php echo $news['image']; ?>" style="width:100px;margin:0 auto;"/>
                            </div>
                        <?php }?>
                        <div class="col-sm-12">
                            <?php echo $news['date_added']; ?>
                        </div>
                        <a href="<?php echo $news['view']; ?>">   
                           <h4 style="color:#2e76b4;"><?php echo $news['title']; ?></h4>
                        </a>
                           <?php echo $news['description']; ?>
                    </div>
                </div>
                <?php } ?>
            </div>
    <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
	  <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 