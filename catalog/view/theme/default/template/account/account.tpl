<?php echo $header; ?>
<style>
    .form-hidden{
        display: none;
    }
    #wl-quantity{
        width: 50px;
        -webkit-appearance: none;
        -o-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
        -ms-appearance: none;
        appearance: none!important;
    }
    .pr_form-group, .as_form-group {
        margin-bottom: 15px;
    }
    .form-horizontal .pr_form-group, .form-horizontal .as_form-group {
        margin-right: -15px;
        margin-left: -15px;
    }
    .pr_form-group:after, .pr_form-group:before,
    .as_form-group:after, .as_form-group:before {
        display: table;
        content: " ";
        clear: both;
    }
</style>

<?php if ($success) { ?>
<div class="alert alert-success"><i
            class="fa fa-check-circle"></i> <?php echo $success; ?></div>
<?php } ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = ' col-sm-12 col-md-9 col-lg-9 '; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            
            <h1><?=$heading_title;?></h1>
            <!--<ul class="nav nav-tabs account_top_menu" role="tablist" style='border:0px solid transparent;'>
                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab"
                                                          data-toggle="tab"><?=$tab_profile;?></a></li>
                <li role="presentation"><a href="#purchase_history" aria-controls="purchase_history" role="tab"
                                           data-toggle="tab"><?=$tab_purchase_history;?></a></li>
                <li role="presentation"><a href="#wish_list" aria-controls="wish_list" role="tab"
                                           data-toggle="tab"><?=$tab_wish_list;?></a></li>
                <li role="presentation"><a href="#comparison_list" aria-controls="comparison_list" role="tab"
                                           data-toggle="tab"><?=$tab_comparison_list;?></a></li>
                <li role="presentation"><a href="<?=$cart_url;?>"><?=$tab_cart;?></a></li>
            </ul> -->
            <nav class="navbar navbar-default" style="background-color: transparent;border-color: transparent;">
                <div class="container-fluid" style="padding:0px;">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header" style="width:100%;">
                    <button type="button" class="navbar-toggle collapsed pull-left buttonAccountTabs" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <h3 class="navbar-brand hidden-lg hidden-md hidden-sm" style="margin-top: 0px;color:black;">Личный кабинет</h3>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2" style="border-color: transparent;padding:0px;overflow-x: hidden;">
                    <ul class="nav navbar-nav">
                        <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab"
                                                                  data-toggle="tab"><?=$tab_profile;?></a></li>
                        <li role="presentation"><a href="#purchase_history" aria-controls="purchase_history" role="tab"
                                                   data-toggle="tab"><?=$tab_purchase_history;?></a></li>
                        <li role="presentation"><a href="#wish_list" aria-controls="wish_list" role="tab"
                                                   data-toggle="tab"><?=$tab_wish_list;?></a></li>
                        <li role="presentation"><a href="#comparison_list" aria-controls="comparison_list" role="tab"
                                                   data-toggle="tab" class="comparison_list_tabs"><?=$tab_comparison_list;?></a></li>
                        <li role="presentation"><a href="<?=$cart_url;?>"><?=$tab_cart;?></a></li>
                    </ul>
                  </div>
              </div>
            </nav>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="profile">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php if ($pr_success) { ?>
                            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $pr_success; ?></div>
                            <?php } ?>
                            <h5 class="account_title" style='font-size:16px;'><b><?=$pr_text_your_details;?></b></h5>
                            
                            <div class='profileForm'>
                                <form action="<?php echo $ac_action; ?>" method="post" enctype="multipart/form-data"
                                      class="form-horizontal">
                                    <div class="pr_form-group required">
                                        <label class="col-sm-3 control-label"
                                               for="input-firstname"><?php echo $pr_entry_firstname; ?> </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="firstname" value="<?php echo $pr_firstname; ?>"
                                                   placeholder="<?php echo $pr_entry_firstname; ?>" id="input-firstname"
                                                   class="form-control"/>
                                            <?php if ($pr_error_firstname) { ?>
                                            <div class="text-danger"><?php echo $pr_error_firstname; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="pr_form-group required">
                                        <label class="col-sm-3 control-label"
                                               for="input-lastname"><?php echo $pr_entry_lastname; ?></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="lastname" value="<?php echo $pr_lastname; ?>"
                                                   placeholder="<?php echo $pr_entry_lastname; ?>" id="input-lastname"
                                                   class="form-control"/>
                                            <?php if ($pr_error_lastname) { ?>
                                            <div class="text-danger"><?php echo $pr_error_lastname; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="pr_form-group required">
                                        <label class="col-sm-3 control-label"
                                               for="input-email"><?php echo $pr_entry_email; ?></label>
                                        <div class="col-sm-9">
                                            <input type="email" name="email" value="<?php echo $pr_email; ?>"
                                                   placeholder="<?php echo $pr_entry_email; ?>" id="input-email"
                                                   class="form-control"/>
                                            <?php if ($pr_error_email) { ?>
                                            <div class="text-danger"><?php echo $pr_error_email; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="pr_form-group required">
                                        <label class="col-sm-3 control-label"
                                               for="input-telephone"><?php echo $pr_entry_telephone; ?></label>
                                        <div class="col-sm-9">
                                            <input type="tel" name="telephone" value="<?php echo $pr_telephone; ?>"
                                                   placeholder="<?php echo $pr_entry_telephone; ?>" id="input-telephone"
                                                   class="form-control"/>
                                            <?php if ($pr_error_telephone) { ?>
                                            <div class="text-danger"><?php echo $pr_error_telephone; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="pr_form-group" style="display: none;">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="fax" value="<?php echo $fax; ?>"
                                                   placeholder="<?php echo $entry_fax; ?>" id="input-fax"
                                                   class="form-control"/>
                                        </div>
                                    </div>

                                    <?php foreach ($custom_fields as $custom_field) { ?>
                                    <?php if ($custom_field['location'] != 'address' && $custom_field['location'] == 'account') { ?>
                                    <?php if ($custom_field['type'] == 'select') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                    id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                    class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                        selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'radio') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <div>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                <div class="radio">
                                                    <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                    <label>
                                                        <input type="radio"
                                                               name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                               value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                               checked="checked"/>
                                                        <?php echo $custom_field_value['name']; ?></label>
                                                    <?php } else { ?>
                                                    <label>
                                                        <input type="radio"
                                                               name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                               value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                        <?php echo $custom_field_value['name']; ?></label>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'checkbox') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <div>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                <div class="checkbox">
                                                    <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $account_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <label>
                                                        <input type="checkbox"
                                                               name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                               value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                               checked="checked"/>
                                                        <?php echo $custom_field_value['name']; ?></label>
                                                    <?php } else { ?>
                                                    <label>
                                                        <input type="checkbox"
                                                               name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                               value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                        <?php echo $custom_field_value['name']; ?></label>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'text') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                   placeholder="<?php echo $custom_field['name']; ?>"
                                                   id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                   class="form-control"/>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'textarea') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                    <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5"
                                              placeholder="<?php echo $custom_field['name']; ?>"
                                              id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                              class="form-control"><?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'file') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <button type="button"
                                                    id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                    data-loading-text="<?php echo $text_loading; ?>"
                                                    class="btn btn-default"><i
                                                        class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : ''); ?>"/>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'date') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <div class="input-group date">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="YYYY-MM-DD"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'time') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <div class="input-group time">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="HH:mm"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'datetime') { ?>
                                    <div class="pr_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-3 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-9">
                                            <div class="input-group datetime">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="YYYY-MM-DD HH:mm"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                    <input type="hidden" name="action_profile" value="edit">
                                    <div class="buttons clearfix">
                                        <div class="pull-left">
                                            <div class='col-sm-6'>
                                                <input type="submit" value="Сохранить" class="btn"  style='background-color:#2a307b !important;border-radius: 0px;color:white !important;border:0px solid transparent;' />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="account_title" style='font-size:16px;'><b><?=$ps_text_your_password;?></b></h5>
                            <?php if ($ps_success) { ?>
                            <div class="alert alert-success"><i
                                        class="fa fa-check-circle"></i> <?php echo $ps_success; ?></div>
                            <?php } ?>
                            <form action="<?php echo $ac_action; ?>" method="post" enctype="multipart/form-data"
                                  class="form-horizontal">
                                <div class="form-group required">
                                    <label class="col-sm-12 control-label" for="input-password" style="text-align: left"><?php echo $ps_entry_password; ?></label>
                                    <div class="col-sm-12">
                                        <input type="password" name="password" value="<?php echo $ps_password; ?>"
                                               placeholder="<?php echo $ps_entry_password; ?>" id="input-password"
                                               class="form-control"/>
                                        <?php if ($ps_error_password) { ?>
                                        <div class="text-danger"><?php echo $ps_error_password; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-12 control-label pull-left" for="input-confirm"  style="text-align: left"><?php echo $ps_entry_confirm; ?></label>
                                    <div class="col-sm-12">
                                        <input type="password" name="confirm" value="<?php echo $ps_confirm; ?>"
                                               placeholder="<?php echo $ps_entry_confirm; ?>" id="input-confirm"
                                               class="form-control"/>
                                        <?php if ($ps_error_confirm) { ?>
                                        <div class="text-danger"><?php echo $ps_error_confirm; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <input type="hidden" name="action_password" value="change">
                                <div class="buttons clearfix">
                                    <div class="pull-left">
                                        <input type="submit" value="<?php echo $button_change; ?>" class="btn" style='background-color:#2a307b !important;border-radius: 0px;color:white !important;border:0px solid transparent;' />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">

                            <?php if ($as_success) { ?>
                            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $as_success; ?></div>
                            <?php } ?>
                            <?php if ($as_error_warning) { ?>
                            <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $as_error_warning; ?></div>
                            <?php } ?>
                            <h5 class="account_title" style='font-size:16px;'><b><?=$as_text_address_book;?></b></h5>
                            <?php if ($addresses) { ?>
                            <div class="row">
                                <?php foreach ($addresses as $result) { ?>
                                    <div class="col-lg-10"><?php echo $result['address']; ?></div>
                                    <div  class="col-lg-2">
                                        <div class="pull-right"><a href="<?php echo $result['delete']; ?>"
                                                  class="btn btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php } else { ?>
                            <p><?php echo $as_text_empty; ?></p>
                            <?php } ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="add_address"><a>+ добавить адрес</a></div>
                                    <div class="add_form well" style='display:none;border:0px solid transparent;border-radius: 0px'>
                                        <form action="<?=$as_action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-firstname"><?php echo $as_entry_firstname; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="firstname" value="<?php echo $as_firstname; ?>" placeholder="<?php echo $as_entry_firstname; ?>" id="input-firstname" class="form-control" />
                                                        <?php if ($as_error_firstname) { ?>
                                                        <div class="text-danger"><?php echo $as_error_firstname; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-lastname"><?php echo $as_entry_lastname; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="lastname" value="<?php echo $as_lastname; ?>" placeholder="<?php echo $as_entry_lastname; ?>" id="input-lastname" class="form-control" />
                                                        <?php if ($as_error_lastname) { ?>
                                                        <div class="text-danger"><?php echo $as_error_lastname; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-company"><?php echo $as_entry_company; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="company" value="<?php echo $as_company; ?>" placeholder="<?php echo $as_entry_company; ?>" id="input-company" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-address-1"><?php echo $as_entry_address_1; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="address_1" value="<?php echo $as_address_1; ?>" placeholder="<?php echo $as_entry_address_1; ?>" id="input-address-1" class="form-control" />
                                                        <?php if ($as_error_address_1) { ?>
                                                        <div class="text-danger"><?php echo $as_error_address_1; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-address-2"><?php echo $as_entry_address_2; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="address_2" value="<?php echo $as_address_2; ?>" placeholder="<?php echo $as_entry_address_2; ?>" id="input-address-2" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="as_form-group required col-sm-12">
                                                    <label class="col-sm-12 control-label" for="input-city" style='text-align: left'><?php echo $as_entry_city; ?></label>
                                                    <div class="col-sm-6" style='padding-left:0px;'>
                                                        <input type="text" name="city" value="<?php echo $as_city; ?>" placeholder="<?php echo $as_entry_city; ?>" id="input-city" class="form-control" />
                                                        <?php if ($as_error_city) { ?>
                                                        <div class="text-danger"><?php echo $as_error_city; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-postcode"><?php echo $as_entry_postcode; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="postcode" value="<?php echo $as_postcode; ?>" placeholder="<?php echo $as_entry_postcode; ?>" id="input-postcode" class="form-control" />
                                                        <?php if ($as_error_postcode) { ?>
                                                        <div class="text-danger"><?php echo $as_error_postcode; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-country"><?php echo $as_entry_country; ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="country_id" id="input-country" class="form-control">
                                                            <option value=""><?php echo $text_select; ?></option>
                                                            <?php foreach ($countries as $country) { ?>
                                                            <?php if ($country['country_id'] == $country_id) { ?>
                                                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                                            <?php } else { ?>
                                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                            <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                        <?php if ($as_error_country) { ?>
                                                        <div class="text-danger"><?php echo $as_error_country; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label" for="input-zone"><?php echo $as_entry_zone; ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="zone_id" id="input-zone" class="form-control">
                                                        </select>
                                                        <?php if ($as_error_zone) { ?>
                                                        <div class="text-danger"><?php echo $as_error_zone; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                        <?php foreach ($as_custom_fields as $custom_field) { ?>
                                                        <?php if ($custom_field['location'] == 'address') { ?>
                                                        <?php if ($custom_field['type'] == 'select') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                                                                    <option value=""><?php echo $text_select; ?></option>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                    <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
                                                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                                    <?php } else { ?>
                                                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                                    <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'radio') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <div>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                    <div class="radio">
                                                                        <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
                                                                        <label>
                                                                            <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                            <?php echo $custom_field_value['name']; ?></label>
                                                                        <?php } else { ?>
                                                                        <label>
                                                                            <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                            <?php echo $custom_field_value['name']; ?></label>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'checkbox') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <div>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                    <div class="checkbox">
                                                                        <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $address_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                        <label>
                                                                            <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                            <?php echo $custom_field_value['name']; ?></label>
                                                                        <?php } else { ?>
                                                                        <label>
                                                                            <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                            <?php echo $custom_field_value['name']; ?></label>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'text') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field <?php if($custom_field['custom_field_id'] == 5){ echo  ' col-sm-6 ' ; }else{ echo ' col-sm-2 '; }?>" data-sort="<?php echo $custom_field['sort_order']; ?>" style='padding-left:5px;padding-right:5px;'>
                                                            <label class="col-sm-12 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" style='text-align: left !important;padding-left:0px;padding-right:0px;'><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-12">
                                                                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'textarea') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-12 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" style='text-align: left'><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-12">
                                                                <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'file') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                                <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'date') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group date">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'time') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group time">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'datetime') { ?>
                                                        <div class="as_form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-3 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group datetime">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                                <?php if (isset($as_error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <div class="text-danger"><?php echo $as_error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php } ?>
                                                        <?php } ?>
                                                <div class="as_form-group form-hidden">
                                                    <label class="col-sm-3 control-label"><?php echo $as_entry_default; ?></label>
                                                    <div class="col-sm-9">
                                                        <?php if ($default) { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="default" value="1" checked="checked" />
                                                            <?php echo $text_yes; ?></label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="default" value="0" />
                                                            <?php echo $text_no; ?></label>
                                                        <?php } else { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="default" value="1" />
                                                            <?php echo $text_yes; ?></label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="default" value="0" checked="checked" />
                                                            <?php echo $text_no; ?></label>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <input type="hidden" name="addAddress" value="true">
                                            <div class="buttons clearfix">
                                                <div class="pull-left">
                                                    <input type="submit" value="<?php echo $as_button_addAddress; ?>" class="btn" style='background-color:#2a307b !important;border-radius: 0px;color:white !important;border:0px solid transparent;' />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="purchase_history">
                    <div class="row ph-wrap-success"></div>
                    <?php
                        $order_date = '';
                        if(!empty($ph_products)){
                        foreach($ph_products as $ph_product){
                            if($order_date !=  $ph_product['date']){
                                $order_date = $ph_product['date'];
                            ?>
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-lg-12">
                                    <div class="order-date"><span style="background-color: #fab446 !important;padding:5px 10px 5px 10px;color:black;font-weight: bold"><?=$ph_product['date'];?></span></div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row" id="ph-product_<?=$ph_product['id'];?>" style="padding-top: 15px;padding-bottom:15px;">
                                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6" style="padding:0px;">
                                    <div class="col-sm-8 col-lg-8 col-md-12">
                                        <div class="row">
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <a href="<?=$ph_product['href'];?>">
                                                    <img src="<?=$ph_product['image'];?>" alt="<?=$ph_product['name'];?>" class="img-responsive" style="margin:0 auto;">
                                                </a>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7" style="text-align: center">
                                                <a href="<?=$ph_product['href'];?>">
                                                    <?=$ph_product['name'];?>
                                                </a>
                                                <?php if(isset($ph_product_attributes)){
                                                    foreach($ph_product_attributes as $ph_product_attribute){
                                                        if(isset($ph_product_attribute[$ph_product['id']])){ ?>
                                                            <div><?=$ph_product_attribute[$ph_product['id']];?></div>
                                                <?php break; } }
                                                }?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-4 col-md-12">
                                        <div class="row order-options" style="text-align: center">
                                            <div class="col-sm-12 col-lg-12" style="padding-left: 0px;padding-right: 0px">
                                                <?php if ($ph_product['options']) { ?>
                                                    <?php foreach ($ph_product['options'] as $option) { ?>
                                                        <?php if ($option['type'] == 'image') { ?>
                                                                <?php if(!empty($option['product_option_value'])){ ?>
                                                                <div class="ph-form-group col-sm-6" style="padding-left: 0px;padding-right:0px;">
                                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                                    <div id="input-option<?php echo $option['product_option_id']; ?>" style="text-align:left">
                                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="" style="width:30px;" /> <?php //echo $option_value['name']; ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="col-lg-12" style="display:none;">
                                                <?php if ($ph_product['options']) { ?>
                                                <?php foreach ($ph_product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'select') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                        <!--<option value=""><?php echo $text_select; ?></option>-->
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'radio') { ?>
                                                <div class="ph-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" checked name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php } ?> 
                                                <!--<?php if ($option['type'] == 'checkbox') { ?>
                                                <div class="ph-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" checked name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php } ?> -->
                                                <?php if ($option['type'] == 'image') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="checkbox" checked name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'text') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'textarea') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'file') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'date') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group date">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'datetime') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group datetime">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'time') { ?>
                                                <div class="ph-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group time">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                                    <div class="row">
                                        <div class="col-sm-3 col-lg-3 col-md-6" style="text-align: center">
                                            <input type="hidden" name="quantity" value="<?=$ph_product['quantity'];?>">
                                            <span><?=$ph_product['quantity'].' шт.';?></span>
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-md-6" style="text-align: center">
                                            <?php if($ph_product['price']){ ?>
                                            <h4 class="price" style="margin-top: 0px"><b><?=$ph_product['price'];?></b></h4>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5 col-lg-5 col-md-12" style="text-align: center">
                                            <input type="hidden" name="product_id" value="<?=$ph_product['id'];?>">
                                            <button type="button" id="ph-button-cart" onclick="ph_cart.add(<?=$ph_product['id'];?>)" class="btn" style="color:white;width:120px;border-radius:0px;background-color: #f15923;border: 0px solid transparent;">
                                                <div class="cart_icon_account" style="float:left;height:20px;width: 20px"></div><span style="padding-left:5px;">В корзину</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>
                    <?php } ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="wish_list">
                    <div class="row wl-wrap-success"></div>
                    <?php if ($wl_success) { ?>
                    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $wl_success; ?>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </div>
                    <?php } ?>
                    <?php if(count($wl_products) > 0){
                        foreach($wl_products as $wl_product){ ?>

                    <div class="row" id="wl-product_<?=$wl_product['id'];?>" style="margin-top:15px;border-bottom: 1px solid #e2e2e2;padding-bottom: 15px">
                        <div class="col-sm-7 col-md-7 col-lg-7">
                            <div class="col-sm-7 col-md-7 col-lg-7">
                                <div class="row">
                                    <div class="col-sm-5 col-md-5 col-lg-5" style="padding: 0px">
                                        <a href="<?=$wl_product['href'];?>">
                                            <img src="<?=$wl_product['image'];?>" alt="<?=$wl_product['name'];?>" class="img-responsive" style="border: 1px solid #e2e2e2;">
                                            <div class="header_heart_icon" style="position:absolute;left:-12px;top:-5px;height:25px;width: 25px"></div>
                                        </a>
                                    </div>
                                    <div class="col-sm-5 col-md-5 col-lg-7">
                                        <a href="<?=$wl_product['href'];?>"><?=$wl_product['name'];?></a>
                                        <?php if(isset($product_attributes[$wl_product['id']])){ ?>
                                        <div><?=$wl_product_attributes[$wl_product['id']];?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5">
                                <div class="row order-options">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <?php if ($wl_product['options']) { ?>
                                            <?php foreach ($wl_product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'select') { ?>
                                                    <div class="wl-form-group">
                                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"  style="color:black;"><?php echo $option['name']; ?></label>
                                                        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow" data-product_id="<?=$wl_product['id'];?>" data-parent="<?php echo $option['product_option_id']; ?>">
                                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        
                                        <?php if ($wl_product['options']) { ?>
                                        <?php foreach ($wl_product['options'] as $option) { ?>
                                        <!--<?php if ($option['type'] == 'select') { ?>
                                        <div class="wl-form-group">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"  style="color:black;"><?php echo $option['name']; ?></label>
                                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php } ?> -->
                                        <?php if ($option['type'] == 'radio') { ?>
                                        <div class="wl-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <!--<?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="wl-form-groupform-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>-->
                                        <?php if ($option['type'] == 'image') { ?>
                                            <?php if (!empty($option['product_option_value'])) { ?>
                                            <div class="wl-form-group">
                                                <div class="row" id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <div class="col-sm-12">
                                                        <label class="control-label" style="color:black;"><?php echo $option['name']; ?></label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <div style='float:left;padding-left:5px;'>
                                                                <label>
                                                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  class="inputColor" style='display:none;' />
                                                                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"  class="img-responsive"  style='height:27px;' /> 
                                                                    <!--<?php echo $option_value['name']; ?>
                                                                    <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                    <?php } ?> -->
                                                                </label>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'text') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'file') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'date') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'time') { ?>
                                        <div class="wl-form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-5 col-md-5  col-lg-5">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4" style="padding:0px;">
                                    <button class="dec-q" style="background-color: #e0e0e0;border-radius: 0px;color:black;border: 0px solid transparent;">-</button><input type="number" id="wl-quantity" min="1" name="quantity" value="1" style="text-align:center;border: 0px solid transparent;"><button class="inc-q" style="background-color: #e0e0e0;border-radius: 0px;color:black;border: 0px solid transparent;">+</button><span>шт.</span>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <?php if($wl_product['price']){ ?>
                                    <h4 class="price" style="margin-top:0px;"><b><?=$wl_product['price'];?></b></h4>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4" style="padding:0px;">
                                    <input type="hidden" name="product_id" value="<?=$wl_product['id'];?>">
                                    <div>
                                        <button type="button" class="btn" id="wl-button-cart" style="background-color: #f15923;border-radius: 0px;color:white;border: 0px solid transparent;" onclick="wl_cart.add(<?=$wl_product['id'];?>)"><div class="cart_icon_account" style="float:left;height:20px;width: 20px"></div><span style="padding-left:5px;">В корзину</span></button>
                                    </div>
                                    <div style="padding-top:8px;">
                                        <a href="<?=$wl_product['remove'];?>" data-toggle="tooltip" title="" data-original-title="Удалить" aria-describedby="tooltip640394" ><i class="glyphicon glyphicon-remove" style="color:red;color:#98070a;font-size:15px;"><span style="padding-left: 6px;color:#387ab0;">Удалить</span></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                     } else { ?>
                    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?=$wl_text_empty;?>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </div>
                    <?php } ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="comparison_list">
                    <?php if ($cm_success) { ?>
                    <div class="alert alert-success"><i
                                class="fa fa-check-circle"></i> <?php echo $cm_success; ?></div>
                    <?php } ?>

                    <ul class="nav nav-tabs menu_comparison_list" id="categ_tab" role="tablist">

                        <?php if($cm_categories){
                            $i = 0;
                            foreach($cm_categories as $cm_category){ ?>
                                <li role="presentation"><a href="#<?=$cm_category['category_id'];?>" aria-controls="<?=$cm_category['category_id'];?>" data-id="<?=$cm_category['category_id'];?>" role="tab" data-toggle="tab" class="sub_menu_sravnit"><?=$cm_category['name'];?></a></li>
                            <?php }
                        } ?>
                    </ul>
                    
                    <?php if ($cm_products_to_category) { ?>
                    <div class="tab-content">
                        
                        <?php foreach($cm_products_to_category as $category => $cm_products){ ?>
                        
                        <div role="tabpanel" class="tab-pane" id="<?=$category;?>">
                            <div class='row'>
                                <div class='col-sm-3 hidden-xs'>
                                    <div class='col-sm-12' style='height:200px;'></div>
                                    <div class='col-sm-12' style='height:55px;'></div>
                                    <div class='col-sm-12' style='height:30px'></div>

                                    <?php foreach ($cm_attribute_groups as $attribute_group) { ?>
                                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                                            <div class='col-sm-12' style='padding:0px;height:30px;'>
                                                <b><?php echo $attribute['name']; ?></b>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class='col-sm-9'>
                                        
                                    <div class="row">
                                        <div class="owl-carousel">
                                            <?php foreach ($cm_products as $product){ ?>
                                                <div class="item" style="width:100% !important;background-color: white">
                                                        <div class='col-sm-12' style='height:200px;padding:0px;'>
                                                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                                        </div>
                                                        <div class='col-sm-12' style='height:55px;padding:0px;'>
                                                            <a href="<?php echo $product['href']; ?>"><strong><?php echo $product['name']; ?></strong></a>
                                                        </div>
                                                        <div class='col-sm-12' style='height:30px;padding:0px;'>
                                                            <?php if ($product['price']) { ?>
                                                                <?php if (!$product['special']) { ?>
                                                                    <h4 style='margin-top: 0px'><b><?php echo $product['price']; ?></b></h4>
                                                                <?php } else { ?>
                                                                    <h4 style='margin-top: 0px'><b><strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?></b></h4>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>

                                                        <?php foreach ($cm_attribute_groups as $attribute_group) { ?>
                                                            <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                                                                <div class='col-sm-12' style='padding:0px;height:30px;'>
                                                                    <?php if (isset($product['attribute'][$key])) { ?>
                                                                        <?php echo $product['attribute'][$key]; ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <div class="col-sm-12" style="padding:0px;">
                                                            <div class="col-sm-12" style="padding:0px;padding-bottom:5px;">
                                                                <button type="button" value="<?php echo $button_cart; ?>" class="btn" style="background-color: #f15923;border-radius: 0px;color:white;border: 0px solid transparent;" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" /><div class="cart_icon_account" style="float:left;height:20px;width: 20px"></div><span style="padding-left:5px;">В корзину</span></button>
                                                            </div>
                                                            <div class="col-sm-12" style="padding:0px;padding-bottom:5px;">
                                                                <a data-toggle="tooltip" title="" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?=$product['product_id'];?>');" data-original-title="В закладки"><div class="header_heart_icon" style="margin-top: 5px;height:15px;width:15px;float:left;"></div><h4 style="margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;">В желания</h4></a>
                                                            </div>
                                                            <div class="col-sm-12" style="padding:0px;">
                                                                <a href="<?php echo $product['remove']; ?>" ><i class="glyphicon glyphicon-remove" style="color:red;color:#98070a;font-size:15px;"><span style="padding-left: 6px;color:#387ab0;">Удалить</span></i></a>
                                                            </div>
                                                        </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <?php }else { ?>
                    <p><?php echo $cm_text_empty; ?></p>
                    <div class="buttons">
                        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
                    </div>
                    <?php } ?>
                </div>

            </div>
            <?php echo $content_bottom; ?></div>
        <div class="hidden-sm"><?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>
