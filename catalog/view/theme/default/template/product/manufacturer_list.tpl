<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($categories) { ?>
      <p><strong><?php echo $text_index; ?></strong>
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
      </p>
      <?php foreach ($categories as $category) { ?>
      <h2 id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h2>
      <?php if ($category['manufacturer']) { ?>
      <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
      <div class="row">
        <?php foreach ($manufacturers as $manufacturer) { ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-2">
                      <img title="<?php echo $manufacturer['name']; ?>" src="../../../image/<?= $manufacturer['image']; ?>" style="height:60px;">
                </div>
                <div class="col-sm-8">
                    <ul class="nav nav-tabs  menu_comparison_list" style='margin-top: 20px'>
                        <?php
                            $categs =  $myData[$manufacturer['id']]['categories'];
                            $k=1;
                            foreach($categs as $key => $value){
                        ?>

                        <li <?= ($k<=1)? 'class="active"' : ''; ?>>
                            <a data-toggle="tab" href="#<?= $manufacturer['id'].'_'.$key ?>">
                                <?= $value ?>
                            </a>
                        </li>
                        <?php $k++ ;} ?>
                    </ul>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="tab-content">
                    <?php
                    $k=1;
                    foreach($categs as $key => $value){ ?>
                      <div id="<?= $manufacturer['id'].'_'.$key ?>" class="tab-pane fade in <?=($k<=1)? 'active' : '';?>">
                        <!--<h3><?= $value ?></h3>-->

                  <?php $products = $myData[$manufacturer['id']]['products'] ?>
                  <?php foreach ($products as $product) {
                    if(in_array($key,$product['categories'])){ ?>
                      <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-left: 5px;padding-right: 5px">

                          <!--отображения в формате grid -->
                            <div class="product-thumb product_grid">
                                <div class="image" style='padding:10px;'>
                                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                                    <?php if ($product['special']) { ?>
                                        <div class='action_product_icon' style='height:50px;width:50px;position:absolute;z-index:99;right:-8px;top:-10px;'></div>
                                   <?php } ?>
                                </div>
                                  <div class="caption col-sm-12" style="min-height:150px;padding-bottom: 10px;padding-left: 7px;padding-right: 7px">
                                          <div class="col-sm-12" style="padding:0px;">
                                              <h4 style="margin-bottom: 2px"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                              <div class='col-sm-12' style='padding:0px;'>
                                                  <?php foreach($product['attribute_groups'] as $product_attribute){
                                                          if($product_attribute['attribute_group_id'] == '7'){
                                                              foreach($product_attribute['attribute'] as $attrib){
                                                                  echo '<span style="color:#727272;">'.$attrib['text'].'</span><br />';
                                                              };
                                                          }
                                                  }; ?>
                                              </div>

                                              <div class='col-sm-12' style='padding:0px;'>
                                                  <?php if (!$product['special']) { ?>
                                                  <?php } else { ?>
                                                      <span class="price-old" style='color:#98070a;'><?php echo $product['price']; ?></span>
                                                  <?php } ?>

                                              </div>
                                              <div class='col-sm-8 col-xs-8 col-md-7' style="padding:0px;">
                                                  <?php if ($product['price']) { ?>
                                                  <p class="price">
                                                    <?php if (!$product['special']) { ?>
                                                    <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price']; ?></span><span style="font-size:9px;" >/полотно</span>
                                                    <?php } else { ?>
                                                    <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special']; ?></span><span >/полотно</span> <!--<span class="price-old"><?php echo $product['price']; ?> </span> -->
                                                    <?php } ?>
                                                  </p>
                                                  <?php } ?>
                                              </div>
                                              <div class='col-sm-4 col-xs-4 col-md-5' style='padding:0px;'>
                                                  <?php if (!$product['special']) { ?>
                                                  <?php if($product['stock'] > 0){ ?>
                                                      <div style='padding:2px 1px 2px 2px;font-size:9px;background-color: #f9a51e;color:black;text-align:center;'><b>в наличии</b></div>
                                                  <?php } ?>
                                                  <?php } ?>
                                              </div>
                                      </div>
                                  </div>
                                  <div class="col-xs-12 hidden-lg hidden-lg hidden-sm" >
                                      <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                                          <div class="col-sm-5" style="padding:2px;">
                                              <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                                          </div>
                                          <div class="col-sm-7" style="padding:2px;">
                                              <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                                          </div>
                                      </div>
                                      <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                                          <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                                              <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                                          </div>
                                          <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                                              <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="product_bottom_info hidden-xs" style='z-index: 100;bottom:-48px;'>
                                      <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                                          <div class="col-sm-5" style="padding:2px;">
                                              <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                                          </div>
                                          <div class="col-sm-7" style="padding:2px;">
                                              <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                                          </div>
                                      </div>
                                      <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                                          <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                                              <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                                          </div>
                                          <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                                              <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                                          </div>
                                      </div>
                                  </div>
                            </div>
                          <!-- конец grid-->
                        </div>
                  <?php }} ?>
                  <!-- <p>Some content.</p> -->
                </div>
                <?php $k++;} ?>

            </div>
        </div>

        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
