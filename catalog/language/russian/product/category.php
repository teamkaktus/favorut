<?php
// Text
$_['text_refine']       = 'Выберите подкатегорию';
$_['text_product']      = 'Товары';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В данной категории нет товаров.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусные баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без НДС:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Название (А - Я)';
$_['text_name_desc']    = 'Название (Я - А)';
$_['text_price_asc']    = 'Цена (низкая &gt; высокая)';
$_['text_price_desc']   = 'Цена (высокая &gt; низкая)';
$_['text_rating_asc']   = 'Рейтинг (начиная с низкого)';
$_['text_rating_desc']  = 'Рейтинг (начиная с высокого)';
$_['text_model_asc']    = 'Модель (А - Я)';
$_['text_model_desc']   = 'Модель (Я - А)';
$_['text_limit']        = 'Показать:';

$_['entry_fo_button']                               = 'Заказ в 1 клик';
$_['entry_fo_name']                               = 'Ваше имя""';
$_['entry_fo_name_error']                               = 'Пожалуйста, напишите как к Вам обращаться';
$_['entry_fo_email_error']                               = 'Пожалуйста, сообщите свой Email';
$_['entry_fo_phone']                               = 'Ваш телефон:';
$_['entry_fo_phone_error']                               = 'Пожалуйста, сообщите свой номер телефона';
$_['entry_fo_message']                               = 'Ваш вопрос:';
$_['entry_fo_message_error']                               = 'Пожалуйста, задайте свой вопрос или оставьте комментарий к заказу.';
$_['entry_fo_close']                               = 'Закрыть';
$_['entry_fo_send']                               = 'Заказать в 1 клик!';
$_['entry_fo_send_success']                               = 'Ваш заказ успешно оформлен!<br />Менеджер свяжется с Вами в ближайшее время.';
$_['entry_fo_send_error']                               = 'Заказ не может быть оформлен.<br />Свяжитесь с нами любым другим удобным способом.';
