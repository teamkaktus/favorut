<?php
// HTTP
define('HTTP_SERVER', 'http://grumax.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://grumax.dev/');

// DIR
define('DIR_APPLICATION', 'C:/wamp/www/grumax/catalog/');
define('DIR_SYSTEM', 'C:/wamp/www/grumax/system/');
define('DIR_LANGUAGE', 'C:/wamp/www/grumax/catalog/language/');
define('DIR_TEMPLATE', 'C:/wamp/www/grumax/catalog/view/theme/');
define('DIR_CONFIG', 'C:/wamp/www/grumax/system/config/');
define('DIR_IMAGE', 'C:/wamp/www/grumax/image/');
define('DIR_CACHE', 'C:/wamp/www/grumax/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/wamp/www/grumax/system/storage/download/');
define('DIR_LOGS', 'C:/wamp/www/grumax/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/wamp/www/grumax/system/storage/modification/');
define('DIR_UPLOAD', 'C:/wamp/www/grumax/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'grumax');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
