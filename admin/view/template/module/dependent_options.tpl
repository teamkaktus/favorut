<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
		  <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-import" data-toggle="tab"><?php echo $tab_import; ?></a></li>
            <li><a href="#tab-license" data-toggle="tab">License</a></li>
            <li><a href="#tab-about" data-toggle="tab">About</a></li>
          </ul>
		  <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
			  <?php echo $text_no_config; ?>
			</div>
			<div class="tab-pane" id="tab-import">
			  <?php if ($import) { ?>
			  <?php include_once(DIR_TEMPLATE . 'module/dependent_options_import.tpl'); ?>
			  <?php } else { ?>
			  <a href="http://www.marketinsg.com/dependent-options-import-export" class="btn btn-success" target="_blank"><?php echo $text_purchase_import; ?></a>
			  <?php } ?>
			</div>
			<?php require_once(DIR_TEMPLATE . 'module/dependent_options_about.tpl'); ?>
		  </div>
		</form>
      </div>
    </div>
	<div style="color:#222222;text-align:center;"><?php echo $heading_title; ?> v<?php echo $version; ?> by <a href="http://www.marketinsg.com" target="_blank">MarketInSG</a></div>
  </div>
</div>
<?php echo $footer; ?>