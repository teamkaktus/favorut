<?php echo $header; ?><?php echo $column_left; ?>
<script>
  var aData = <?= $aData ?>
</script>
<div id="content" class="container">
  <input type="hidden" id="dir_image" value="<?php echo DIR_IMAGE?>">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-contact" data-toggle="tooltip" title="<?php //echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php //echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php //echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-sm-6">
      <form action="<?php echo $action; ?>" name="contact"  method="post" id="form-contact"  class="form-horizontal">

      </form>

    </div>
    <div class="col-sm-6">
      <h2>Click to Map to place marker</h2>
      <canvas id="canvas" height="700" width=700 style="z-index:9"></canvas>
    </div>
  </div>
</div>
<fieldset id="template" style="display:none">

<!-- Form Name -->
<legend><span class="heading_legnd">Заполните поля</span><button type="button" id="" class="cross close" aria-label="Close"><span aria-hidden="true">&times;</span></button></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="title[]">Название</label>
  <div class="col-md-8">
  <input id="title[]" name="title[]" type="text" placeholder="Название" class="form-control title input-md" required="">

  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="data[]">Основная информацыя</label>
  <div class="col-md-8">
    <textarea class="form-control"  name="data[]"></textarea>
  </div>
</div>
<input type="hidden" class="xpos" name="xpos[]" value="">
<input type="hidden" class="ypos" name="ypos[]" value="">
<div class="table-responsive">
  <table id="images" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-left">Картинка</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      <tr>
        <td></td>
        <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="Добавить" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
      </tr>
    </tfoot>
  </table>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="adress[]">Адрес точки на карту</label>
  <div class="col-md-8 pa">
    <input id="adress[]" name="adress[]" type="text" placeholder="Адрес" class="form-control adress input-md" required="">
    <input id="adress_db[]" name="adress_db[]" type="hidden" placeholder="Адрес" class="form-control adress_db input-md" required="">

  </div>
</div>
</fieldset>

<?php echo $footer; ?>
