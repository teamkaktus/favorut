function addImage(k) {
	html  = '<tr id="image-row' + image_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img  src="/image/cache/no_image-100x100.png" alt="" title="" data-placeholder="" /><input type="hidden" name="image['+k+'][]" value="no_image.png" id="input-image' + image_row + '" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#images'+k+' tbody').append(html);

	image_row++;
}
function addImageStartup(k,src) {
	html  = '<tr id="image-row' + image_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img width="100px" height="100px" style="object-fit: contain;" src="/image/'+src+'" alt="" title="" data-placeholder="" /><input type="hidden" name="image['+k+'][]" value="'+src+'" id="input-image' + image_row + '" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#images'+k+' tbody').append(html);

	image_row++;
}
function getIndexByAttribute(list, attr, val){
    var result = null;
    $.each(list, function(index, item){
        if(item[attr].toString() == val.toString()){
           result = index;
           return false;     // breaks the $.each() loop
        }
    });
    return result;
}
function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}
  var image_row = 0
$(document).ready(function() {

  var canvas = document.getElementById('canvas');
  var context = canvas.getContext("2d");
  var k = 0;
  var canvasOffset = $("#canvas").offset();
  var offsetX = canvasOffset.left;
  var offsetY = canvasOffset.top;
  var mapSprite = new Image();
  mapSprite.src = "../../../../image/contact_map.png";

  var Marker = function () {
      this.Sprite = new Image();
      this.Sprite.src = "../../../../image/contact_marker.png"
      this.Width = 24;
      this.Height = 40;
      this.XPos = 0;
      this.YPos = 0;
  }
  var Markers = new Array();

  var mouseClicked = function (mouse) {
      // Get corrent mouse coords
      var rect = canvas.getBoundingClientRect();
      var mouseXPos = (mouse.x - rect.left);
      var mouseYPos = (mouse.y - rect.top);

      // Move the marker when placed to a better location
      var marker = new Marker();
      marker.XPos = mouseXPos - (marker.Width / 2);
      marker.YPos = mouseYPos - marker.Height;
      marker.id = k
      k++
      Markers.push(marker);
      var template = $('#template').clone()
      template.html(template.html().replace(/\[\]/g,'['+k+']').replace(/addImage\(\)/g,'addImage('+k+')').replace(/id="images"/g,'id="images'+k+'"'))
      template.css('display','block')
      template.attr('id', marker.id)
      template.find('.heading_legnd').text(template.find('.heading_legnd').text()+' для маркера '+ (marker.id+1))
      template.find('.cross').attr('id', (marker.id))
      template.find('textarea').addClass('data');
      template.find('textarea').attr('id', (marker.id))
      template.find('textarea.data').summernote({height: 300,default:''});
      template.find('.xpos').val(marker.XPos)
      template.find('.ypos').val(marker.YPos)
      template.find('.adress').geocomplete().bind("geocode:result", function(event, result){
        $(this).parent('.pa').find('.adress_db').val(result.formatted_address)
      });
      $('#form-contact').append(template)
      delete template

  }
  $('form').submit(function (e) {;
    $('textarea.data').each(function () {
      $(this).text($(this).closest('div').find('.note-editable').html())
    })
  if(!$('form')[0].checkValidity())
    e.preventDefault()
      // $('form').submit()
    // if($('form').)

  })
  $(document).on('click','.cross',function () {
    id = $(this).attr('id')
    id2 = getIndexByAttribute(Markers,'id',id)
    Markers.splice(id2,1)
    $('fieldset#'+id).remove()
  })
  // Add mouse click event listener to canvas
  canvas.addEventListener("mousedown", mouseClicked, false);

  var firstLoad = function () {
      context.font = "15px Georgia";
      context.textAlign = "center";
      $.each(aData,function (key,value) {
        k++;
        var marker = new Marker()
        marker.id = decodeEntities(value.title)
        marker.XPos = value.xpos
        marker.YPos = value.ypos
        Markers.push(marker)
        var template = $('#template').clone()
        template.html(template.html().replace(/\[\]/g,'['+k+']').replace(/addImage\(\)/g,'addImage('+k+')').replace(/id="images"/g,'id="images'+k+'"'))
        template.css('display','block')
        template.attr('id',Markers.indexOf(marker))
        template.find('.heading_legnd').text(template.find('.heading_legnd').text()+' для маркера '+ (marker.id))
        template.find('.cross').attr('id', (Markers.indexOf(marker)))
        template.find('textarea').addClass('data');
        template.find('textarea').attr('id', (Markers.indexOf(marker)))
        template.find('textarea.data').summernote({height: 300,defaultText:''});
        template.find('.note-editable').html(decodeEntities(value.description))
        template.find('.title').val(decodeEntities(value.title))
        template.find('.xpos').val(marker.XPos)
        template.find('.ypos').val(marker.YPos)
        template.find('.adress_db').val(value.adress_db)
        template.find('.adress').geocomplete().bind("geocode:result", function(event, result){
          $(this).parent('.pa').find('.adress_db').val(result.formatted_address)
        });
        template.find('.adress').val(template.find('.adress_db').val()).trigger("geocode");
        $('#form-contact').append(template)
        delete template
        if(value.images != null){
          $.each(value.images,function(key,value) {
            addImageStartup(k,value)
          })
        }
      })
  }
  firstLoad();
  // $('.adress').geocomplete().bind("geocode:result", function(event, result){
  //   $(this).parent('.pa').find('.adress_db').val(result.formatted_address)
  // });

  var main = function () {
      draw();
  };

  var draw = function () {
      // Clear Canvas
      context.fillStyle = "#FFF";
      context.fillRect(0, 0, canvas.width, canvas.height);

      // Draw map
      // Sprite, X location, Y location, Image width, Image height
      // You can leave the image height and width off, if you do it will draw the image at default size
      context.drawImage(mapSprite, 0, 0, 700, 700);

      // Draw markers
      for (var i = 0; i < Markers.length; i++) {
          var tempMarker = Markers[i];
          // Draw marker
          context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);

          // Calculate postion text
          var markerText = "Marker "+(tempMarker.id+1);

          // Draw a simple box so you can see the position
          var textMeasurements = context.measureText(markerText);
          context.fillStyle = "#666";
          context.globalAlpha = 0.7;
          context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 15, textMeasurements.width, 20);
          context.globalAlpha = 1;

          // Draw position above
          context.fillStyle = "#000";
          context.fillText(markerText, tempMarker.XPos, tempMarker.YPos);
      }
  };
  setInterval(main, (1000 / 60)); // Refresh 60 times a second
  $('.txt').summernote({height: 300});
})
