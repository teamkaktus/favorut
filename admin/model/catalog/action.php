<?php
class ModelCatalogAction extends Model {
    public function addAction($data) {
        $this->event->trigger('pre.admin.action.add', $data);

        $this->db->query("INSERT INTO " . DB_PREFIX . "action SET sort_order = '" . (int)$data['sort_order'] . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "'");

        $action_id = $this->db->getLastId();

        foreach ($data['action_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', short_description = '" . $this->db->escape($value['short_description']) ."', description = '" . $this->db->escape($value['description']) . "', image = '" . $this->db->escape($value['image']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['product_action'])) {
            foreach ($data['product_action'] as $product_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "action_product WHERE action_id = '" . (int)$action_id . "' AND product_id = '". (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_product SET action_id = '" . (int)$action_id . "', product_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['action_store'])) {
            foreach ($data['action_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_to_store SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['action_layout'])) {
            foreach ($data['action_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_to_layout SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('action');

        $this->event->trigger('post.admin.action.add', $action_id);

        return $action_id;
    }

    public function editAction($action_id, $data) {
        $this->event->trigger('pre.admin.action.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "action SET sort_order = '" . (int)$data['sort_order'] . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "' WHERE action_id = '" . (int)$action_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");

        foreach ($data['action_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', short_description = '" . $this->db->escape($value['short_description']) ."', description = '" . $this->db->escape($value['description']) . "', image = '" . $this->db->escape($value['image']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "action_product WHERE action_id = '" . (int)$action_id . "'");

        if (isset($data['product_action'])) {
            foreach ($data['product_action'] as $product_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_product SET action_id = '" . (int)$action_id . "', product_id = '" . (int)$product_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");

        if (isset($data['action_store'])) {
            foreach ($data['action_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_to_store SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");

        if (isset($data['action_layout'])) {
            foreach ($data['action_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "action_to_layout SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'action_id=" . (int)$action_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('action');

        $this->event->trigger('post.admin.action.edit', $action_id);
    }

    public function deleteAction($action_id) {
        $this->event->trigger('pre.admin.action.delete', $action_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'action_id=" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "action_product WHERE action_id='" . (int)$action_id . "'");

        $this->cache->delete('action');

        $this->event->trigger('post.admin.action.delete', $action_id);
    }

    public function getAction($action_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = action_id='" . (int)$action_id . "') AS keyword FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");

        return $query->row;
    }

    public function getActions($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "action i LEFT JOIN " . DB_PREFIX . "action_description id ON (i.action_id = id.action_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sort_data = array(
                'id.title',
                'i.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $information_data = $this->cache->get('action.' . (int)$this->config->get('config_language_id'));

            if (!$information_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action i LEFT JOIN " . DB_PREFIX . "action_description id ON (i.action_id = id.action_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

                $information_data = $query->rows;

                $this->cache->set('action.' . (int)$this->config->get('config_language_id'), $information_data);
            }

            return $information_data;
        }
    }

    public function getActionProducts($action_id) {
        $actions_product_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_product WHERE action_id = '" . (int)$action_id . "'");

        foreach ($query->rows as $result) {
            $actions_product_data[] = $result['product_id'];
        }

        return $actions_product_data;
    }

    public function getActionsDescriptions($action_id) {
        $action_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");

        foreach ($query->rows as $result) {
            $action_description_data[$result['language_id']] = array(
                'title'            => $result['title'],
                'short_description'=> $result['short_description'],
                'description'      => $result['description'],
                'image'            => $result['image'],
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword']
            );
        }

        return $action_description_data;
    }

    public function getActionStores($action_id) {
        $information_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");

        foreach ($query->rows as $result) {
            $information_store_data[] = $result['store_id'];
        }

        return $information_store_data;
    }

    public function getActionLayouts($action_id) {
        $information_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");

        foreach ($query->rows as $result) {
            $information_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $information_layout_data;
    }

    public function getTotalActions() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action");

        return $query->row['total'];
    }

    public function getTotalActionsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }
}